const FontType = {
  regular: "Mulish-Regular",
  bold: "Mulish-Bold",
  SemiBold: "Mulish-SemiBold",
  ExtraBold: "Mulish-ExtraBold"
};
export default FontType;
