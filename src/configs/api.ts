import axios from "axios";
import { ENV } from ".";

const host = axios.create({
  baseURL: ENV.host,
  timeout: 5000
});
export const newhost = axios.create({
  baseURL: ENV.newHost,
  timeout: 5000
});
const getcsrf = () =>
  host.get("/api/csrf-token").then(
    response => {
      return response.data.data.csrfToken;
    },
    error => {
      return error;
    }
  );
const getTryoutCart = (jwt: string) =>
  host.get("/api/shops/cart", {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`
    }
  });
const getMyTryout = (jwt: string) =>
  host.get("/api/users/tryouts", {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`
    }
  });
const postjwt = (csrf: string, refreshtoken: string) =>
  host.post(
    "/api/users/refresh-token",
    {
      token: refreshtoken
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf
      }
    }
  );
const Login = (email: string, password: string, csrf: string) =>
  host.post(
    "/api/users/authenticate",
    {
      email,
      password
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf
      }
    }
  );
const Logins = (url: string) => axios.get(url);
const addtochart = (tryoutId: string, jwt: string, csrf: string) =>
  host.post(
    "/api/shops/",
    {
      tryoutId
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf,
        Authorization: `Bearer ${jwt}`
      }
    }
  );
const removeproductchart = (tryoutId: string, jwt: string, csrf: string) =>
  host.post(
    "/api/shops/remove-product",
    {
      tryoutId
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf,
        Authorization: `Bearer ${jwt}`
      }
    }
  );
const postcheckout = (jwt: string, csrf: string) =>
  host.post(
    "/api/shops/checkout",
    {},
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf,
        Authorization: `Bearer ${jwt}`
      }
    }
  );
const getuserorders = (jwt: string) =>
  host.get("/api/users/orders", {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`
    }
  });
const   register = (
  name: string,
  formasi: string,
  provinsi: string,
  kota: string,
  phone: string,
  id: string,
  token: string
) =>
  newhost.put(
    `/api/users/${id}`,
    {
      Name: name,
      formation: formasi,
      Provence: provinsi,
      City: kota,
      Phone_number: phone,
      level: 1,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: token
      }
    }
  );
const forgotpassword = (email: string, csrf: string) =>
  host.post(
    "/api/users/forgot-password",
    {
      email
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf
      }
    }
  );
const logout = (jwt: string, csrf: string, refreshtoken: string) =>
  host.post(
    "/api/users/revoke-token",
    {
      token: refreshtoken
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf,
        Authorization: `Bearer ${jwt}`
      }
    }
  );
const getCategoryTryout = (jwt: string, category: string) =>
  host.get(`/api/categories/descendants/${category}`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`
    }
  });
const getListTryout = (jwt: string, category: string) =>
  host.get(`/api/tryouts/category/${category}`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`
    }
  });
const getTryout = (jwt: string, slug: string) =>
  host.get(`/api/tryout-exams/${slug}`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwt}`
    }
  });
const getDetailTryout = (jwt: string, slug: string) =>
  host.get(`/api/tryouts/${slug}`, {
    headers: {
      "Content-Type": "application/json",
      "csrf-token": "3Od3KXBl-jrv_B4Fiy9i04YQSN2sAeTPCous", // auth.login.csrf
      Authorization: `Bearer ${jwt}`
    }
  });
const postTryout = (answer: any[], csrf: string, jwt: string) =>
  host.post(
    "/api/tryout-exams/assessment",
    {
      tryoutId: answer.tryoutId,
      examId: answer.examId,
      questions: answer.questions,
      time: answer.time
    },
    {
      headers: {
        "Content-Type": "application/json",
        "csrf-token": csrf,
        Authorization: `Bearer ${jwt}`
      }
    }
  );
const api = {
  getSeasons: () => host.get("seasons"),
  getCsrf: () => getcsrf(),
  Login: (url: string) => Logins(url),
  getToken: (email: string, password: string, csrf: string) =>
    Login(email, password, csrf),
  postRegister: (
    name: string,
    formasi: string,
    provinsi: string,
    kota: string,
    phone: string,
    id: string,
    token: string
  ) => register(name, formasi, provinsi, kota, phone, id, token),
  postForgotPassword: (email: string, csrf: string) =>
    forgotpassword(email, csrf),
  postLogout: (jwt: string, csrf: string, refreshtoken: string) =>
    logout(jwt, csrf, refreshtoken),
  getListTryout: (jwt: string, category: string) =>
    getListTryout(jwt, category),
  getTryout: (jwt: string, slug: string) => getTryout(jwt, slug),
  getDetailTryout: (jwt: string, slug: string) => getDetailTryout(jwt, slug),
  getCategoryTryout: (jwt: string, slug: string) =>
    getCategoryTryout(jwt, slug),
  postTryout: (answer: any[], csrf: string, jwt: string) =>
    postTryout(answer, csrf, jwt),
  postjwt: (csrf: string, refreshtoken: string) => postjwt(csrf, refreshtoken),
  addtochart: (tryoutId: string, jwt: string, csrf: string) =>
    addtochart(tryoutId, jwt, csrf),
  getTryoutCart: (jwt: string) => getTryoutCart(jwt),
  removeproductchart: (tryoutId: string, jwt: string, csrf: string) =>
    removeproductchart(tryoutId, jwt, csrf),
  postcheckout: (jwt: string, csrf: string) => postcheckout(jwt, csrf),
  getMyTryout: (jwt: string) => getMyTryout(jwt),
  getuserorders: (jwt: string) => getuserorders(jwt)
};
export default api;
