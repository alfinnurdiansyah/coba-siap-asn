const images = {
  logo: require("../assets/images/logo.png"),
  logowelcome: require("../assets/images/welcomepage.png"),
  imagelogin: require("../assets/images/imagelogin.png"),
  siapasn: require("../assets/images/siapasn.png"),
  iconHome: require("../assets/images/iconHome.png"),
  bgLogin: require("../assets/images/bglogin.png"),
  splash: require("../assets/images/splash.png"),
  bgregister: require("../assets/images/bgregister.png"),
  bgcontach: require("../assets/images/Saly.png"),
  info: require("../assets/images/Info.png"),
  lock: require("../assets/images/Lock.png"),
  exit: require("../assets/images/exit.png"),
  exitBlue: require("../assets/images/exitBlue.png"),
  bgProfil: require("../assets/images/bgProfil.png"),
  fb: require("../assets/images/Facebook.png"),
  ig: require("../assets/images/Instagram.png"),
  yt: require("../assets/images/YouTube.png"),
  gambar: require("../assets/images/gambar.png"),
  poster: require("../assets/images/poster.png")
};

export default images;
