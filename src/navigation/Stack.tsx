import React from "react";
import {
  TransitionPresets,
  createStackNavigator,
} from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import SplashScreen from "../screens/SplashScreen";
import Setting from "../screens/Setting";
import i18n from "../I18n";
import Auth from "../screens/Login";
import Paket from "../screens/Paket";
import StudentHome from "../screens/StudentHome";
import Materi from "../screens/StudentMateri";
import About from "../screens/About";
import SubMateri from "../screens/StudentSubMateri";
import Profil from "../screens/Profil";
import Leaderboard from "../screens/Leaderboard";
import {
  Bag,
  Chart,
  Home as IconHome,
  InfoCircle,
  PaperNegative
} from "react-native-iconly";
const { Navigator, Screen } = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const Home = () => {
  return (
    <Tab.Navigator
      initialRouteName="StudentHome"
      activeColor="#F5C114"
      inactiveColor="#ABABAB"
      barStyle={{ backgroundColor: "#ffffff" }}
    >
      <Tab.Screen
        name="StudentHome"
        component={StudentHome}
        options={{
          tabBarLabel: "Try Out",
          tabBarIcon: ({ focused }) => (
            <IconHome
              filled
              color={focused ? "#F5C114" : "#ABABAB"}
              size={20}
            />
          )
        }}
      />
      <Tab.Screen
        name="Materi"
        component={Materi}
        options={{
          tabBarLabel: "Materi",
          tabBarIcon: ({ focused }) => (
            <PaperNegative
              filled
              color={focused ? "#F5C114" : "#ABABAB"}
              size={20}
            />
          )
        }}
      />
      <Tab.Screen
        name="Paket"
        component={Paket}
        options={{
          tabBarLabel: "Paket",
          tabBarIcon: ({ focused }) => (
            <Bag filled color={focused ? "#F5C114" : "#ABABAB"} size={20} />
          )
        }}
      />
      <Tab.Screen
        name="Peringkat"
        component={Leaderboard}
        options={{
          tabBarLabel: "Peringkat",
          tabBarIcon: ({ focused }) => (
            <Chart filled color={focused ? "#F5C114" : "#ABABAB"} size={20} />
          )
        }}
      />
      <Tab.Screen
        name="TentangKami"
        component={About}
        options={{
          tabBarLabel: "Tentang Kami",
          tabBarIcon: ({ focused }) => (
            <InfoCircle color={focused ? "#F5C114" : "#ABABAB"} size={20} />
          )
        }}
      />
    </Tab.Navigator>
  );
};
const Stack = () => (
  <Navigator
    initialRouteName="SplashScreen"
    screenOptions={{ ...TransitionPresets.SlideFromRightIOS }}
  >
    <Screen
        name="SplashScreen"
        component={SplashScreen.Home}
        options={{ header: () => null }}
      />
    <Screen name="Home" component={Home} options={{ header: () => null }} />
    <Screen
      name="Setting"
      component={Setting}
      options={{ title: i18n.t("setting") }}
    />
    <Screen
        name="HomeLogin"
        component={Auth.Home}
        options={{ header: () => null }}
      />
      <Screen
        name="Register"
        component={Auth.Register}
        options={{ header: () => null }}
      />
      <Screen
        name="Profil"
        component={Profil}
      />
      <Screen
        name="SubMateri"
        component={SubMateri}
        options={{ header: () => null }}
      />
      <Screen
        name="ForgotPassword"
        component={Auth.ForgotPassword}
        options={{ header: () => null }}
      />
      
  </Navigator>
);

export default Stack;
