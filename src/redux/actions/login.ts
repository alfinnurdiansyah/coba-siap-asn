import { Alert } from "react-native";
import { Dispatch, GetState } from "../types";
import { API } from "../../configs";
import { GoogleSignin } from "@react-native-google-signin/google-signin";
import { newhost } from "../../configs/api";

export const LOGOUT = "LOGOUT";
export const GET_CSRF_PENDING = "GET_CSRF_PENDING";
export const GET_CSRF_SUCCESS = "GET_CSRF_SUCCESS";
export const GET_CSRF_ERROR = "GET_CSRF_ERROR";
export const GET_JWT_PENDING = "GET_JWT_PENDING";
export const GET_JWT_SUCCESS = "GET_JWT_SUCCESS";
export const GET_JWT_ERROR = "GET_JWT_ERROR";
export const LOGIN_PENDING = "LOGIN_PENDING";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN = "LOGIN";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const REGISTER_PENDING = "REGISTER_PENDING";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_ERROR = "REGISTER_ERROR";
export const FORGOT_PASSWORD_PENDING = "FORGOT_PASSWORD_PENDING";
export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";
export const FORGOT_PASSWORD_ERROR = "FORGOT_PASSWORD_ERROR";
export const SAVE_LINK = "SAVE_LINK";

export const getCSRF = (cb?: (csrf: string) => void) => async (
  dispatch: Dispatch
) => {
  try {
    dispatch({ type: GET_CSRF_PENDING });
    const res = await API.getCsrf();
    cb && cb(res);
    dispatch({
      type: GET_CSRF_SUCCESS,
      payload: { data: res }
    });
  } catch (err) {
    if (err.response) {
      dispatch({ type: GET_CSRF_ERROR, payload: { data: err.response } });
    } else {
      dispatch({ type: GET_CSRF_ERROR });
    }
  }
};
export const postRefreshToken = (
  cb?: (jwt: string) => void,
  csrf?: string
) => async (dispatch: Dispatch, getState: GetState) => {
  const { login } = getState();
  try {
    dispatch({ type: GET_JWT_PENDING });
    const res = await API.postjwt(csrf || login.csrf, login.refreshtoken);
    cb && cb(res.data.data.jwtToken);
    dispatch({
      type: GET_JWT_SUCCESS,
      payload: { data: res.data.data }
    });
  } catch (err) {
    if (err.response) {
      switch (err.response.status) {
        case 403:
          dispatch(
            getCSRF(e => {
              dispatch(postRefreshToken(cb, e));
            })
          );
          break;
        default:
          dispatch({
            type: GET_JWT_ERROR,
            payload: { data: err.response.data }
          });
          break;
      }
    } else {
      dispatch({ type: GET_JWT_ERROR });
    }
  }
};
export const register = (
  name: string,
  formasi: string,
  provinsi: string,
  kota: string,
  phone: string,
  id: string,
  token: string,
  navigation: any
) => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: REGISTER_PENDING });
    await API.postRegister(
      name,
      formasi,
      provinsi,
      kota,
      phone,
      id,
      token
    );
    await newhost.get("api/users/me", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(res => {
      dispatch({
        type: LOGIN,
        payload: {
          data: {
            jwtToken: token,
            user: res.data
          }
        }
      });
    });
    navigation.replace("ForgotPassword");
  } catch (err) {
    Alert.alert("", "Pendaftaran Gagal");
  }
};
export const login = (
  email: string,
  password: string,
  csrf: string,
  navigation: any
) => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: LOGIN_PENDING });
    const response = await API.getToken(email, password, csrf);
    dispatch({ type: LOGIN_SUCCESS, payload: { data: response.data } });
    navigation.navigate("Home");
  } catch (error) {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          dispatch({ type: LOGIN_ERROR });
          Alert.alert("", "Username atau Password Salah");
          break;
        case 403:
          dispatch(
            getCSRF(e => {
              dispatch(login(email, password, e, navigation));
            })
          );
          break;
        case 422:
          Alert.alert("", "format email tidak valid");
          break;
        default:
          dispatch({
            type: LOGIN_ERROR,
            payload: { data: error.response.data }
          });
          Alert.alert("", "Username atau Password Salah");
          break;
      }
    } else {
      dispatch({ type: LOGIN_ERROR });
      Alert.alert("", "Username atau Password Salah");
    }
  }
};
export const logout = (
  navigation: any,
) => async (dispatch: Dispatch, getState: GetState) => {
  try {
    await GoogleSignin.signOut();
    dispatch({ type: LOGOUT, payload: { data: "" } });
    navigation.replace("HomeLogin");
  } catch (err) {
    Alert.alert("Gagal",JSON.stringify(err))
  }
};
export const forgotpassword = (
  email: string,
  csrf: string,
  navigation: any
) => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: FORGOT_PASSWORD_PENDING });
    const res = await API.postForgotPassword(email, csrf);
    dispatch({ type: FORGOT_PASSWORD_SUCCESS, payload: { data: res } });
    Alert.alert("", "Silahkan Cek Email Anda", [
      {
        text: "OK",
        onPress: () => navigation("Login")
      }
    ]);
  } catch (err) {
    if (err.response) {
      if (err.response.status === 403) {
        dispatch(
          getCSRF(e => {
            dispatch(forgotpassword(email, e, navigation));
          })
        );
      }
    } else {
      dispatch({ type: FORGOT_PASSWORD_ERROR });
    }
  }
};
