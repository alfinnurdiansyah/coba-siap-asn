import { combineReducers } from "redux";

import persist from "./persist";
import home from "./home";
import login from "./login"

export default combineReducers({
  persist,
  home,
  login
});
