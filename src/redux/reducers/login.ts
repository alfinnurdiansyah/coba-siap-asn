import {
  FORGOT_PASSWORD_ERROR,
  FORGOT_PASSWORD_PENDING,
  FORGOT_PASSWORD_SUCCESS,
  GET_CSRF_ERROR,
  GET_CSRF_PENDING,
  GET_CSRF_SUCCESS,
  GET_JWT_SUCCESS,
  LOGIN,
  LOGIN_ERROR,
  LOGIN_PENDING,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER_ERROR,
  REGISTER_PENDING,
  REGISTER_SUCCESS,
  SAVE_LINK,
} from "../actions";
import { Action, LoginState } from "../types";

const initialState: LoginState = {
  csrf: "",
  jwttoken: "",
  refreshtoken: "",
  isLoadingLogin: false,
  isLoadingRegister: false,
  isLoadingForgotPassword: false,
  user: {},
  link: {}
};
const Login = (state = initialState, { type, payload }: Action) => {
  switch (type) {
    case SAVE_LINK:
      return { ...state, link: payload.data };
    case LOGOUT:
      return { ...state, jwttoken: "" };
    case LOGIN_PENDING:
      return { ...state, isLoadingLogin: true };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoadingLogin: false,
        jwttoken: payload.data.data.jwtToken,
        refreshtoken: payload.data.data.refreshToken
      };
    case LOGIN:
      return {
        ...state,
        jwttoken: payload.data.jwtToken,
        user: payload.data.user
      };
    case LOGIN_ERROR:
      return { ...state, isLoadingLogin: false };
    case REGISTER_PENDING:
      return { ...state, isLoadingRegister: true };
    case REGISTER_SUCCESS:
      return { ...state, isLoadingRegister: false };
    case REGISTER_ERROR:
      return { ...state, isLoadingRegister: false };
    case FORGOT_PASSWORD_PENDING:
      return { ...state, isLoadingForgotPassword: true };
    case FORGOT_PASSWORD_SUCCESS:
      return { ...state, isLoadingForgotPassword: false };
    case FORGOT_PASSWORD_ERROR:
      return { ...state, isLoadingForgotPassword: false };
    case GET_CSRF_PENDING:
      return { ...state };
    case GET_CSRF_SUCCESS:
      return { ...state, csrf: payload.data };
    case GET_CSRF_ERROR:
      return { ...state };
    case GET_JWT_SUCCESS:
      return {
        ...state,
        isLoadingLogin: false,
        jwttoken: payload.data.jwtToken,
        refreshtoken: payload.data.refreshToken
      };
    default:
      return state;
  }
};
export default Login;
