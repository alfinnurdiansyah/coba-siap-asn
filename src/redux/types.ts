// Action
interface Payload {
  data?: any;
}

interface Params {
  type: string;
  payload?: Payload;
}

export type Dispatch = (params: Params | Function) => void;
export type GetState = () => Reducers;

export interface Action {
  type: string;
  payload: Payload;
}

// Reducer
export interface Reducers {
  home: HomeState;
  persist: PersistState;
  login: LoginState;
}

export interface HomeState {
  data: any[];
  isLoadingGetSeason: boolean;
  listSeasons: any[];
}

export interface LoginState {
  csrf: string;
  isLoadingLogin: boolean;
  isLoadingRegister: boolean;
  isLoadingForgotPassword: boolean;
  jwttoken: string;
  refreshtoken: string;
  user: object;
  link:object;
}

export interface PersistState {
  language: string;
}
