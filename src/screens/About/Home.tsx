import React, { useEffect, useState } from "react";
import {
  Alert,
  Image,
  Linking,
  Modal,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import RBSheet from "react-native-raw-bottom-sheet";

import { ENV, IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { Reducers } from "../../redux/types";
import { newhost } from "../../configs/api";

import styles from "./styles";

const Home = () => {
  const login = useSelector((state: Reducers) => state.login);
  const [listMentor, setListMentor] = useState([]);
  const [listTestimoni, setListTestimoni] = useState([]);
  const _HandleGetDataMentor = () => {
    newhost
      .get("api/guest-mentors?populate=*", {
        headers: {
          Authorization: `Bearer ${login.jwttoken}`,
        },
      })
      .then((res) => {
        setListMentor(res.data.data);
      });
  };
  const _HandleGetDataTestimoni = () => {
    newhost
      .get("api/testimonis?populate=*", {
        headers: {
          Authorization: `Bearer ${login.jwttoken}`,
        },
      })
      .then((res) => {
        const data= res.data.data;
        setListTestimoni(data);
      });
  };
  const _handleOpenLink = async(url:any) =>{
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(url);
    } else {
      Alert.alert(`Link Tidak Bisa Dibuka`);
    }
  }
  useEffect(() => {
    _HandleGetDataMentor();
    _HandleGetDataTestimoni();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <ScrollView style={{flex:1}}>
          <View style={{alignItems:'center'}}>
            <Image style={{width:222}} resizeMode="contain" source={IMAGES.siapasn} />
            <Text
              style={{
                color: "#5D5C5D",
                fontFamily: FontType.ExtraBold,
                fontSize: 18,
                textAlign:'center',
              }}
              >
                Safir Academy{'\n'}
                www.safiracademy.net{'\n'}
                {'\n'}
                informasi:
                {'\n'}
            </Text>
            <TouchableOpacity onPress={()=>_handleOpenLink('https://wa.me/+62'+login.link.attributes.Kontak_wa)} style={{alignItems:'center',backgroundColor:'#00CB6A',borderRadius:8,paddingVertical:8,paddingHorizontal:15}}>
              <Text style={{ color: "#FFFFFF",fontSize:14,fontFamily: FontType.ExtraBold, }}>
                Status WA
              </Text>
            </TouchableOpacity>
            <Text style={{ color: "#5D5C5D",fontSize:18,width:width(90),marginTop:15,fontFamily: FontType.ExtraBold, }}>
              Sosial Media:
            </Text>
            <View style={{flexDirection:'row',justifyContent:'space-around',width:width(70),marginTop:20}}>
              <TouchableOpacity onPress={()=>_handleOpenLink(login.link.attributes.Youtube)}>
                <Image style={{width:60,height:60,borderRadius:60}} resizeMode="contain" source={IMAGES.yt} />
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>_handleOpenLink(login.link.attributes.Instagram)}>
                <Image style={{width:60,height:60,borderRadius:60}} resizeMode="contain" source={IMAGES.ig} />
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>_handleOpenLink(login.link.attributes.Facebook)}>
                <Image style={{width:60,height:60,borderRadius:60}} resizeMode="contain" source={IMAGES.fb} />
              </TouchableOpacity>
            </View>
            <Text style={{ color: "#5D5C5D",fontSize:18,width:width(90),marginTop:15,fontFamily: FontType.ExtraBold, }}>
              Guest Mentor
            </Text>
            <ScrollView horizontal style={{width:width(90),marginTop:10}} showsHorizontalScrollIndicator={false}>
              {listMentor.map((res)=>
                <View style={{overflow:'hidden',height:120,width:120,borderRadius:10,backgroundColor:'#FAC40A',marginRight:8}}>
                  <Image style={{flex:1}} resizeMode="contain" source={{uri:ENV.newHost+''+res.attributes.picture.data.attributes.url}} />
                  <View style={{width:'100%',height:40,backgroundColor:'#FDE284',position:'absolute',bottom:0,alignItems:'center',paddingHorizontal:5,justifyContent:'center'}}>
                    <Text numberOfLines={1} style={{ color: "#000000",fontSize:12,fontFamily: FontType.ExtraBold }}>
                      {res.attributes.name}
                    </Text>
                    <Text numberOfLines={1} style={{ color: "#000000",fontSize:10,fontFamily: FontType.ExtraBold, }}>
                      {res.attributes.title}
                    </Text>
                  </View>
                </View>
              )}
            </ScrollView>
            <Text style={{ color: "#5D5C5D",fontSize:18,width:width(90),marginTop:15,fontFamily: FontType.ExtraBold, }}>
              Testimoni
            </Text>
            <ScrollView horizontal style={{width:width(90),marginTop:10}} showsHorizontalScrollIndicator={false}>
              {listTestimoni.map((res)=>
                <View style={{overflow:'hidden',height:120,width:120,borderRadius:10,backgroundColor:'#FAC40A',marginRight:8}}>
                  {res.attributes.content.data&&<Image style={{flex:1}} resizeMode="cover" source={{uri:ENV.newHost+res.attributes.content.data.attributes.url}} />}
                </View>
              )}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
export default Home;
