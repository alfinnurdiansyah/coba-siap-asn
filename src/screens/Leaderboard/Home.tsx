import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  FlatList,
  Picker
} from "react-native";
import styles from "./styles";
import {
  LineChart,
} from "react-native-chart-kit";
import { newhost } from "../../configs/api";
import { useSelector } from "react-redux";
import { Reducers } from "../../redux/types";
import width from "../../utils/widthPercent";
import FontType from "../../assets/fonts";
const Home = () => {
  const login = useSelector((state: Reducers) => state.login);
  const screenWidth = Dimensions.get("window").width;
  const [dataset, setDataset] = useState([]);
  const [labels, setLabels] = useState([]);
  const [selectedExam, setSelectedExam] = useState("Pedagogik");
  const [selectedValue, setSelectedValue] = useState("Formasi");
  const [listPeringkat, setListPeringkat] = useState([]);
  const [listExam, setListExam] = useState(["Pedagogik","Manajerial","Sosio Kultural","Wawancara"]);
  const chartConfig = {
    backgroundGradientFrom: "#FFFFFF",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#FFFFFF",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(239, 190, 36, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional
    decimalPlaces: 0,
    labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  };
  const _HandleGetHitory = (examName:any) => {
    newhost
      .get("/api/records?exam="+examName, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        var data: any[] = [];
        var name: any[] = [];
        res.data.Histories.map((item:any)=>{
          data.push(item.Score)
          name.push(item.Order)
        })
        setDataset(data);
        setLabels(name);
      });
  };
  const _HandleGetLeaderboard = (value:any) => {
    if(value==="Formasi"){
      newhost
      .get("api/leaderboard-formation?formasi="+login.user.formation.Name, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        setListPeringkat(res.data)
      }).catch(()=>{
        setListPeringkat([])
      });
    }else{
      newhost
      .get("api/leaderboards?province="+login.user.Provence+"&city="+login.user.City, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        setListPeringkat(res.data)
      }).catch(()=>{
        setListPeringkat([])
      });
    }
  };
  useEffect(() => {
    _HandleGetHitory(selectedExam);
  }, [selectedExam]);
  useEffect(() => {
    _HandleGetLeaderboard(selectedValue);
  }, [selectedValue]);
  const _renderItem = ({ item, index }: any) => (
    <View style={{flexDirection:'row',alignItems:'center',marginBottom:15}}>
      <Text
        style={{
        flex:0.2,
        fontSize: 16,
        fontFamily: FontType.SemiBold,
        color:  "#5D5C5D",
        }}
      >
        {index + 1}
      </Text>
      <View style={{width:25,height:25,borderRadius:25,backgroundColor:'blue',marginHorizontal:20}}>
      </View>
      <Text
        style={{
        fontSize: 16,
        fontFamily: FontType.SemiBold,
        color:  "#5D5C5D",
        flex:2.5,
        }}
      >
        {item.user.username}
      </Text>
      <Text
        style={{
        fontSize: 16,
        fontFamily: FontType.SemiBold,
        color:  "#5D5C5D",
        flex:1,
        }}
      >
        {item.final_score}
      </Text>
    </View>
  )
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          <View style={styles.body}>
            <View
              style={{ width: width(93) }}
            >
                <View
                  style={{
                    backgroundColor: "#FFFFFF",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    alignItems: "center",
                    overflow: "hidden",
                  }}
                >
                   <Text
                      style={{
                      fontSize: 14,
                      fontFamily: FontType.SemiBold,
                      color:  "#5D5C5D",
                      marginBottom:10
                      }}
                    >
                      Grafik Peningkatan
                    </Text>
                    {dataset.length>0&&<LineChart
                    data={{
                      labels: labels,
                      datasets: [
                        {
                          data: dataset,
                          color: (opacity = 1) => `rgba(239, 190, 36,${opacity})`,
                          strokeWidth: 2,
                        },
                        // {
                        //   data: [0] // min
                        // },
                      ],
                    }}
                    width={screenWidth}
                    height={220}
                    chartConfig={chartConfig}
                    bezier
                  />}
                  
                  <ScrollView
                    style={{
                      width: width(93),
                      marginTop:10,
                    }}
                    pagingEnabled
                    horizontal
                    showsHorizontalScrollIndicator={false}
                  >
                    {listExam.map((res) => (
                      <TouchableOpacity
                        onPress={()=>setSelectedExam(res)}
                        style={{
                          paddingHorizontal: 12,
                          paddingVertical: 8,
                          backgroundColor: res===selectedExam?"#FAC40A":"#E2E0E0",
                          borderRadius: 10,
                          marginRight:8,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 12,
                            fontFamily: FontType.bold,
                            color:  res===selectedExam?"#FFFFFF":"#ABABAB",
                          }}
                        >
                          {res}
                        </Text>
                      </TouchableOpacity>
                    ))}
                  </ScrollView>
                </View>
              </View>
              <View
                style={{ width: width(93) }}
              >
                <View
                  style={{
                    backgroundColor: "#FFFFFF",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    overflow: "hidden",
                  }}
                >
                  <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                    <Text
                      style={{
                      fontSize: 14,
                      fontFamily: FontType.SemiBold,
                      color:  "#5D5C5D",
                      }}
                    >
                    Peringkat Berdasarkan 
                    </Text>
                    <Picker
                      selectedValue={selectedValue}
                      style={{ height: 50, width: 150 }}
                      onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                    >
                      <Picker.Item label="Formasi" value="Formasi" />
                      <Picker.Item label="Daerah" value="Daerah" />
                    </Picker>
                  </View>
                <View style={{height:0.3,width:'100%',backgroundColor:'#BEBEBE',marginVertical:10}}/>
                <FlatList
                  keyboardShouldPersistTaps="handled"
                  data={listPeringkat}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={_renderItem}
                  ListEmptyComponent={()=>(
                  <View>
                     <Text
                      style={{
                      fontSize: 14,
                      fontFamily: FontType.SemiBold,
                      color:  "#5D5C5D",
                      textAlign:"center"
                      }}
                    >
                    Peringkat Belum Tersedia 
                    </Text>
                  </View>
                  )}
                />
              </View>
            </View>
          </View>
      </View>
    </SafeAreaView>
  );
};
export default Home;
