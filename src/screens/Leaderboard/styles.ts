import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F7F7"
  },
  body: {
    width: "100%",
    alignItems: "center"
  },
});

export default styles;
