import React from "react";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  PermissionsAndroid,
  Platform,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Contacts from "react-native-contacts";
import styles from "./styles";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { IMAGES } from "../../configs";
import { useSelector } from "react-redux";
import { Reducers } from "../../redux/types";

const ForgotPassword = () => {
  const login = useSelector((state: Reducers) => state.login);
  const navigation = useNavigation();
  const _ForgotPassword = () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
      title: "Contacts",
      message: "This app would like to view your contacts.",
      buttonPositive: "Please accept bare mortal"
    }).then(() => {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts.",
        buttonPositive: "Please accept bare mortal"
      }).then(() => {
        const newPerson = {
          familyName: "Safir Academy",
          givenName: "",
          phoneNumbers: [
            {
              label: "mobile",
              number: login.link.attributes.Kontak_wa
            }
          ]
        };
        Contacts.addContact(newPerson);
        navigation.replace("Home");
      });
    });
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View>
          <Image style={styles.bgcontach} source={IMAGES.bgcontach} />
          <Text
            style={{
              fontFamily: FontType.bold,
              color: "#152C07",
              fontSize: 24,
              textAlign: "center",
              width: width(85),
              alignSelf: "center",
              marginTop: 40
            }}
          >
            Selangkah lagi, kamu bisa akses informasi dari SiapASN
          </Text>
          <Text
            style={{
              fontFamily: FontType.SemiBold,
              color: "#ABABAB",
              fontSize: 16,
              textAlign: "center",
              width: width(90),
              marginTop: 10,
              alignSelf: "center"
            }}
          >
            Simpan kontak SiapASN agar kamu dapat mengakses kabar terbaru
            seputar penerimaan PPPK
          </Text>
          <TouchableHighlight
            onPress={() => _ForgotPassword()}
            style={styles.submitButton}
          >
            <Text
              style={{
                fontSize: 12,
                fontFamily: FontType.SemiBold,
                color: "white"
              }}
            >
              Simpan Kontak
            </Text>
          </TouchableHighlight>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};
export default ForgotPassword;
