import React, { useRef, useState } from "react";
import {
  Image,
  ImageBackground,
  Modal,
  Platform,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import { WebView } from "react-native-webview";
import styles from "./styles";
import { API, COLORS, IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import { LOGIN } from "../../redux/actions";
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import DeviceInfo from 'react-native-device-info';
import { newhost } from "../../configs/api";
const Home = () => {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const dispatch = useDispatch();
  const _HandleGetProfil = (jwt:any,response:any) => {
      newhost.get("api/users/me", {
        headers: {
          Authorization: `Bearer ${jwt}`,
        },
      }).then(res => {
        dispatch({
          type: LOGIN,
          payload: {
            data: {
              jwtToken: jwt,
              user: res.data
            }
          }
        });
        setModalVisible(false);
      const data = response.data.user;
      if (
          data.Phone_number ||
          data.Provence ||
          data.City ||
          data.Name
        ) {
          navigation.replace("Home");
        } else {
          navigation.replace("Register");
        }
      });
    };
  const handleLogin=async()=>{
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const response = await API.Login('https://api.siapasn.com/api/auth/google/callback?access_token='+userInfo.idToken);
      _HandleGetProfil(response.data.jwt,response)
    } catch (error:any) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
      }
    }
  }
  
  React.useEffect(() => {
    GoogleSignin.configure({
      webClientId: '461873518360-6fr6nusbr477h624d03d8fetbqsb2loi.apps.googleusercontent.com',
      offlineAccess: true,
    });
  }, [])
  return (
    <View style={styles.container}>
      <ImageBackground style={styles.bodyhome} source={IMAGES.bgLogin}>
        <View style={styles.body}>
          <Image style={styles.logo} source={IMAGES.siapasn} />
          <Text
            style={{
              fontFamily: FontType.bold,
              color: "#152C07",
              fontSize: 32
            }}
          >
            Selamat Datang
          </Text>
          <Text
            style={{
              fontFamily: FontType.SemiBold,
              color: "#0B7BF9",
              fontSize: 14,
              marginTop: 4
            }}
          >
            Calon PPPK
          </Text>
          <TouchableOpacity
            onPress={() => handleLogin()}
            style={{
              backgroundColor: "white",
              borderRadius: 16,
              paddingHorizontal: 30,
              paddingVertical: 5,
              marginVertical: 60
            }}
          >
            <Text>Masuk Dengan Google</Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View
            style={{ backgroundColor: "blue", height: "100%", width: "100%" }}
          >
            <WebView
              source={{ uri: "https://api.siapasn.com/api/connect/google" }}
              style={{
                height: "100%",
                width: "100%"
              }}
              onNavigationStateChange={async navState => {
                // Keep track of going back navigation within component
                if (
                  navState.url.includes(
                    "api.siapasn.com/api/auth/google/callback/"
                  )
                ) {
                  const response = await API.Login(navState.url);
                  dispatch({
                    type: LOGIN,
                    payload: {
                      data: {
                        jwtToken: response.data.jwt,
                        user: response.data.user
                      }
                    }
                  });
                  setModalVisible(false);
                  const data = response.data.user;
                  if (
                    data.Phone_number ||
                    data.Provence ||
                    data.City ||
                    data.Name
                  ) {
                    navigation.replace("Home");
                  } else {
                    navigation.replace("Register");
                  }
                }
              }}
              userAgent={DeviceInfo.getUserAgent() + " - siapasn - android "}  
            />
          </View>
        </Modal>
      </ImageBackground>
    </View>
  );
};
export default Home;
