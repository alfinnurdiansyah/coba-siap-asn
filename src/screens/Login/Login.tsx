import React, { useState } from "react";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { useDispatch, useSelector } from "react-redux";

import styles from "./styles";
import I18n from "../../I18n";
import { login } from "../../redux/actions";
import { IMAGES } from "../../configs";
import { Reducers } from "../../redux/types";

const Login = () => {
  const csrf = useSelector((state: Reducers) => state.login);
  const [email, setEmail] = useState("");
  const [password, setPasword] = useState("");
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const _Login = () => {
    dispatch(login(email, password, csrf.csrf, navigation));
    setPasword("");
    setEmail("");
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.body}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="arrow-back" size={30} />
            </TouchableOpacity>
            <Image
              style={{
                width: 100,
                height: 78,
                marginVertical: 20,
                marginStart: 15,
                resizeMode: "cover"
              }}
              source={IMAGES.imagelogin}
            />
            <View style={{ marginHorizontal: 15 }}>
              <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                Masukkan email dan kata sandimu untuk login
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "bold",
                  marginTop: 20
                }}
              >
                {I18n.t("email")}
              </Text>
              <TextInput
                placeholder={I18n.t("email")}
                autoCompleteType="email"
                onChangeText={text => setEmail(text)}
                value={email}
                keyboardType="email-address"
                style={{ borderBottomWidth: 0.6 }}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "bold",
                  marginTop: 20
                }}
              >
                {I18n.t("password")}
              </Text>
              <TextInput
                style={{ borderBottomWidth: 0.6 }}
                placeholder={I18n.t("password")}
                onChangeText={text => setPasword(text)}
                secureTextEntry
                value={password}
              />
            </View>
            <TouchableOpacity
              style={{
                marginTop: 10,
                marginBottom: 10,
                alignItems: "center",
                justifyContent: "center"
              }}
              onPress={() => navigation.navigate("ForgotPassword")}
            >
              <Text style={{ color: "#0000ff", marginTop: 5 }}>
                {I18n.t("forgotpasword")}
              </Text>
            </TouchableOpacity>
          </ScrollView>
          <TouchableHighlight
            disabled={email === "" || password === ""}
            onPress={() => _Login()}
            style={styles.submitButton}
          >
            <Text style={styles.submitButtonText}>{I18n.t("login")}</Text>
          </TouchableHighlight>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default Login;
