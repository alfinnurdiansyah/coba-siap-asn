import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { Picker } from "@react-native-community/picker";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import styles from "./styles";
import { Reducers } from "../../redux/types";
import { register } from "../../redux/actions";
import { IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { newhost } from "../../configs/api";

const Register = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [formasi, setFormasi] = useState("");
  const [provinsi, setProfinsi] = useState("Jawa Timur");
  const [kota, setKota] = useState("Surabaya");
  const [listFormasi, setListFormasi] = useState([]);
  const login = useSelector((state: Reducers) => state.login);
  const navigation = useNavigation();

  const _Register = () => {
    dispatch(
      register(
        name,
        formasi,
        provinsi,
        kota,
        phone,
        login.user.id,
        login.jwttoken,
        navigation
      )
    );
  };
  const _HandleGetData = () => {
    newhost.get("api/formations").then(res => {
      setListFormasi(res.data.data);
    });
  };
  useEffect(() => {
    _HandleGetData();
  }, []);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ImageBackground style={styles.bodyhome} source={IMAGES.bgregister}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View
              style={{
                width: width(100),
                paddingHorizontal: 20,
                marginTop: 25
              }}
            >
              <Text
                style={{
                  fontFamily: FontType.bold,
                  color: "#152C07",
                  fontSize: 24,
                  textAlign: "center",
                  width: width(85),
                  alignSelf: "center",
                  marginTop: 40
                }}
              >
                Yeay, kamu adalah pengguna baru!
              </Text>
              <Text
                style={{
                  fontFamily: FontType.SemiBold,
                  color: "#ABABAB",
                  fontSize: 16,
                  textAlign: "center",
                  width: width(90),
                  marginTop: 10,
                  alignSelf: "center"
                }}
              >
                Silakan melengkapi data berikut untuk melanjutkan
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontType.SemiBold,
                  marginTop: 20
                }}
              >
                Nama
              </Text>
              <TextInput
                placeholder={"Nama Lengkap"}
                autoCompleteType="email"
                onChangeText={text => setName(text)}
                value={name}
                style={{
                  backgroundColor: "white",
                  padding: 12,
                  marginTop: 8,
                  borderRadius: 8
                }}
              />
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontType.SemiBold,
                  marginTop: 20
                }}
              >
                Whatsapp
              </Text>
              <TextInput
                placeholder={"Nomor Whatsapp"}
                autoCompleteType="tel"
                keyboardType="phone-pad"
                onChangeText={text => setPhone(text)}
                value={phone}
                style={{
                  backgroundColor: "white",
                  padding: 12,
                  marginTop: 8,
                  borderRadius: 8
                }}
              />
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontType.SemiBold,
                  marginTop: 20
                }}
              >
                Formasi Tujuan
              </Text>
              <View
                style={{
                  backgroundColor: "white",
                  paddingHorizontal: 12,
                  marginTop: 8,
                  borderRadius: 8
                }}
              >
                <Picker
                  selectedValue={formasi}
                  style={{ height: 45 }}
                  onValueChange={(itemValue, itemIndex) => {
                    setFormasi(itemValue);
                  }}
                >
                  {listFormasi.map(item => (
                    <Picker.Item
                      label={item.attributes.Name}
                      value={item.id}
                      key={item.id}
                    />
                  ))}
                </Picker>
              </View>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontType.SemiBold,
                  marginTop: 20
                }}
              >
                Provinsi
              </Text>
              <View
                style={{
                  backgroundColor: "white",
                  paddingHorizontal: 12,
                  marginTop: 8,
                  borderRadius: 8
                }}
              >
                <Picker
                  selectedValue={provinsi}
                  style={{ height: 45 }}
                  onValueChange={(itemValue, itemIndex) =>
                    setProfinsi(itemValue)}
                >
                  <Picker.Item label="Jawa Timur" value="Jawa Timur" />
                </Picker>
              </View>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontType.SemiBold,
                  marginTop: 20
                }}
              >
                Kota/Kabupaten
              </Text>
              <View
                style={{
                  backgroundColor: "white",
                  paddingHorizontal: 12,
                  marginTop: 8,
                  borderRadius: 8
                }}
              >
                <Picker
                  selectedValue={kota}
                  style={{ height: 45 }}
                  onValueChange={(itemValue, itemIndex) => setKota(itemValue)}
                >
                  <Picker.Item label="Surabaya" value="Surabaya" />
                </Picker>
              </View>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontType.SemiBold,
                  marginTop: 20
                }}
              >
                Saya setuju akan
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: FontType.SemiBold,
                    color: "#1E82C5"
                  }}
                >
                  {" "}
                  Syarat dan Ketentuan, Kebijakan Privasi
                </Text>
{" "}
                dan
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: FontType.SemiBold,
                    color: "#1E82C5"
                  }}
                >
                  {" "}
                  Kebijakan Konten
                </Text>
              </Text>
              <TouchableHighlight
                disabled={
                  phone === "" ||
                  name === "" ||
                  formasi === "" ||
                  kota === "" ||
                  provinsi === ""
                }
                onPress={() => _Register()}
                style={{ ...styles.submitButton, alignSelf: "center" }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: FontType.SemiBold,
                    color: "white"
                  }}
                >
                  Daftar
                </Text>
              </TouchableHighlight>
            </View>
          </ScrollView>
        </ImageBackground>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};
export default Register;
