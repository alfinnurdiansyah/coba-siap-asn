import Login from "./Login";
import ForgotPassword from "./ForgotPassword";
import Register from "./Register";
import Home from "./Home";

const Auth = {
  Login,
  ForgotPassword,
  Register,
  Home
};
export default Auth;
