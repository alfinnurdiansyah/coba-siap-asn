import { StyleSheet } from "react-native";

import { COLORS } from "../../configs";
import { widthPercent } from "../../utils";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.background,
    alignItems: "center"
  },
  logo: {
    width: 200,
    resizeMode: "contain",
    marginBottom: 30
  },
  loading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center"
  },
  bodyhome: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  bgcontach: {
    width: 199,
    height: 199,
    alignSelf: "center",
    marginTop: 100
  },
  body: {
    alignItems: "center",
    marginTop: -50
  },
  submitButton: {
    backgroundColor: "#F5C114",
    paddingHorizontal: 23,
    paddingVertical: 11,
    alignItems: "center",
    marginTop: 24,
    borderRadius: 4
  }
});

export default styles;
