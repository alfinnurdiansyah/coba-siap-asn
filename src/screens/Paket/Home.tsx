import React, { useEffect, useState } from "react";
import {
  Image,
  Modal,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Alert
} from "react-native";
import { useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import CountDown from 'react-native-countdown-component';
import { IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import { WebView } from 'react-native-webview';
import { Reducers } from "../../redux/types";
import { newhost } from "../../configs/api";
import moment from "moment";
import styles from "./styles";
import width from "../../utils/widthPercent";
import height from "../../utils/heightPercent";

const Home = () => {
  const login = useSelector((state: Reducers) => state.login);
  const [listPaket, setListPaket] = useState([]);
  const [url, setURL] = useState("");
  const [durasi, setDurasi] = useState("");
  const [modalPaymentVisible, setModalPaymentVisible] =useState(false)
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const _HandleGetDataPaket = () => {
    newhost
      .get("api/packages?populate=*", {
        headers: {
          Authorization: `Bearer ${login.jwttoken}`,
        },
      })
      .then((res) => {
        setListPaket(res.data);
      });
  };
  const _formatRupiah = (angka:any, prefix:any)=>{
    var number_string = angka.replace(/[^,\d]/g, '').toString();
    var split = number_string.split(',');
    var sisa = split[0].length % 3;
    var rupiah	= split[0].substr(0, sisa);
    var ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
  
    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
      var separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
  
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
  const _handleBuyPaket = (paket:any) => {
    var level = 0
    if(paket==="Hemat"){
      level=1
    }else if(paket==="Premium"){
      level=2
    }else if(paket==="Bimbingan"){
      level=3
    }
    setModalPaymentVisible(false);
    setIsLoading(true);
    if(login.user.level.Level_akun>level)
    {
      newhost
      .get("api/transactions?package="+paket, {
        headers: {
          Authorization: `Bearer ${login.jwttoken}`,
        },
      })
      .then((res) => {
        setURL(res.data.transactionRedirectUrl);
        setModalVisible(true)
        setIsLoading(false);
      });
    }else{
      if(login.user.level.Level_akun===level){
        Alert.alert("Gagal","Anda Sudah Berada Di Paket Ini");
      }else{
        Alert.alert("Gagal","Tidak Bisa Downgrade")
      }
    }
  }
  useEffect(() => {
    var a = moment(new Date());
    var b = moment(login.user.Durasi);
    setDurasi(b.diff(a, 'days'))
    _HandleGetDataPaket();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        {isLoading?
          (<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <ActivityIndicator size="large" color="#0000ff"/>
          </View>
          ):(
          <ScrollView>
          <View style={styles.body}>
            <TouchableOpacity
              style={{
                marginTop:40,
                alignItems:'center'
              }}
              onPress={()=>navigation.navigate("Profil")}
            >
              <View style={{
                width:80,
                height:80,
                borderRadius:80,
                backgroundColor:'blue'
              }}>
              </View>
              <Text
                style={{
                  fontFamily: FontType.bold,
                  color: "#152C07",
                  fontSize: 24,
                  marginTop: 8,
                }}>
                  {login.user.Name}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                backgroundColor:'#F5C114',
                width:'100%',
                alignItems:'center',
                borderRadius:10,
                paddingVertical:10,
                marginTop:50,
              }}
            >
              <View
                style={{
                  width:265,
                  height:65,
                  borderRadius:16,
                  position:'absolute',
                  top:-30,
                  overflow:'hidden',
                  backgroundColor:'white',
                  flexDirection:'row',
                  alignItems:'center'
                }}
              >
                <View style={{
                  flex:1,
                  alignItems:'center'
                }}>
                  <Text
                    style={{
                      fontFamily: FontType.SemiBold,
                      color: "#0B7BF9",
                      fontSize: 10,
                    }}>
                      Paket Kamu:
                  </Text>
                  <View
                    style={{
                      backgroundColor:'#E0FFE5',
                      paddingHorizontal:12,
                      paddingVertical:4,
                      borderRadius:12,
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: FontType.ExtraBold,
                        color: "#00CB6A",
                        fontSize: 12,
                      }}>
                        {login.user.level.Name}
                    </Text>
                  </View>
                </View>
                <View style={{
                  height:35,
                  width:1,
                  backgroundColor:'#0B7BF9'
                }}/>
                <View style={{
                  flex:1,
                  alignItems:'center'
                }}>
                  <Text
                    style={{
                      fontFamily: FontType.SemiBold,
                      color: "#0B7BF9",
                      fontSize: 10,
                    }}>
                      Sisa Hari:
                  </Text>
                  <Text
                    style={{
                      fontFamily: FontType.ExtraBold,
                      color: "#0B7BF9",
                      fontSize: 24,
                    }}>
                      {login.user.Durasi ? durasi:'∞'}
                  </Text>
                </View>
              </View>
              <Text
                style={{
                  fontFamily: FontType.SemiBold,
                  color: "#5D5C5D",
                  fontSize: 16,
                  marginTop: 38,
                  marginBottom:15
                }}
              >
                Promo akan berakhir dalam:
              </Text>
              <CountDown
                until={10000}
                size={20}
                digitStyle={{backgroundColor: '#FFF',height:30,width:30,borderRadius:10,marginHorizontal:5}}
                digitTxtStyle={{fontSize: 15,color: 'black',fontFamily: FontType.regular,}}
                timeLabels={{d: 'Hari',h: 'Jam',m: 'Menit', s: 'Detik'}}
              />
            </View>
            {parseInt(login.user.level.Level_akun)<=2 &&
              <Text 
                style={{
                  marginVertical:24,
                  fontFamily: FontType.ExtraBold,
                  color: "#5D5C5D",
                  fontSize: 14,
                }}
              >
                Tentukan paket INTENSIF pilihanmu
              </Text>
            }
            {parseInt(login.user.level.Level_akun)<=1 &&(
              <View
              style={{
                width:'100%',
                alignItems:'center',
                borderRadius:10,
                overflow:'hidden',
              }}
            >
              <View
                style={{
                  height:132,
                  backgroundColor:'#F5C114',
                  width:'100%',
                }}
              >
              </View>
              <View
                style={{
                  paddingBottom:15,
                  backgroundColor:'white',
                  width:'100%',
                  alignItems:'center',
                  paddingTop:26
                }}
              >
                <Image
                  style={{
                    height: 50,
                    width: 50,
                    position:'absolute',
                    top:-25
                  }}
                  resizeMode="contain"
                  source={IMAGES.gambar}
                />
                <Text 
                  style={{
                    fontFamily: FontType.ExtraBold,
                    color: "#0B7BF9",
                    fontSize: 18,
                  }}
                >
                  PAKET PPPK
                </Text>
                <Text 
                  style={{
                    fontFamily: FontType.regular,
                    color: "#5D5C5D",
                    fontSize: 14,
                  }}
                >
                  Terlatih menghadapi soal PPPK
                </Text>
                <TouchableOpacity
                  onPress={()=>setModalPaymentVisible(true)}
                  style={{
                    backgroundColor:'#0B7BF9',
                    paddingHorizontal:8,
                    paddingVertical:4,
                    borderRadius:8,
                    marginTop: 10,
                  }}
                >
                  <Text 
                    style={{
                      fontFamily: FontType.bold,
                      color: "white",
                      fontSize: 12,
                    }}
                  >
                    Lihat Paket
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            )}
            {parseInt(login.user.level.Level_akun)<=2 && (
              <TouchableOpacity
              style={{
                marginTop:24,
                padding:12,
                borderRadius:10,
                backgroundColor:'#59A3F6',
                flexDirection:'row',
              }}
              onPress={()=>_handleBuyPaket(listPaket[2].Name)}
            >
              <View style={{flex:1}}>
                <Text 
                  style={{
                    fontFamily: FontType.bold,
                    color: "white",
                    fontSize: 18,
                  }}
                >
                  BIMBINGAN MAPEL
                </Text>
                <Text 
                  style={{
                    fontFamily: FontType.regular,
                    color: "white",
                    fontSize: 16,
                  }}
                >
                  {listPaket.length>2 && listPaket[2].Description}
                </Text>
              </View>
              <View style={{flex:0.5,paddingTop:20}}>
                <Text 
                  style={{
                    fontFamily: FontType.bold,
                    color: "white",
                    fontSize: 13,
                    textDecorationLine:'line-through',
                    textAlign:'right'
                  }}
                >
                  {listPaket.length>2 &&_formatRupiah(listPaket[2].Normal_Price, 'Rp. ')}
                </Text>
                <Text 
                  style={{
                    fontFamily: FontType.bold,
                    color: "#F5C114",
                    fontSize: 18,
                    textAlign:'right'
                  }}
                >
                  {listPaket.length>2 &&_formatRupiah(listPaket[2].Discount_Price, 'Rp. ')}
                </Text>
                <View 
                  style={{
                    backgroundColor: '#F5C114',
                    paddingHorizontal: 16,
                    paddingVertical: 4,
                    borderRadius: 8,
                    marginTop: 8,
                    alignSelf:'flex-end'
                  }}
                >
                  <Text 
                    style={{
                      fontFamily: FontType.bold,
                      color: "white",
                      fontSize: 12,
                    }}
                  >
                    Upgrade
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            )}
            {parseInt(login.user.level.Level_akun)<=1 && (
            <TouchableOpacity
              style={{
                marginTop:24,
                padding:12,
                borderRadius:10,
                backgroundColor:'white',
                flexDirection:'row',
              }}
              onPress={()=>_handleBuyPaket(listPaket[parseInt(login.user.level.Level_akun)].Name)}
            >
              <View style={{flex:1}}>
                <Text 
                  style={{
                    fontFamily: FontType.bold,
                    color: "#0B7BF9",
                    fontSize: 18,
                  }}
                >
                  Upgrade paket
                </Text>
                <Text 
                  style={{
                    fontFamily: FontType.regular,
                    color: "#5D5C5D",
                    fontSize: 16,
                  }}
                >
                  Ayo Tingkatkan Belajarmu
                </Text>
              </View>
              <View style={{flex:0.5,paddingTop:20}}>
                <Text 
                  style={{
                    fontFamily: FontType.bold,
                    color: "#0B7BF9",
                    fontSize: 13,
                    textDecorationLine:'line-through',
                    textAlign:'right'
                  }}
                >
                  {listPaket.length > 0 &&_formatRupiah(listPaket[parseInt(login.user.level.Level_akun)].Normal_Price, 'Rp. ')}
                </Text>
                <Text 
                  style={{
                    fontFamily: FontType.bold,
                    color: "#F5C114",
                    fontSize: 18,
                    textAlign:'right'
                  }}
                >
                  {listPaket.length > 0 &&_formatRupiah(listPaket[parseInt(login.user.level.Level_akun)].Discount_Price, 'Rp. ')}
                </Text>
                <View 
                  style={{
                    backgroundColor: '#F5C114',
                    paddingHorizontal: 16,
                    paddingVertical: 4,
                    borderRadius: 8,
                    marginTop: 8,
                    alignSelf:'flex-end'
                  }}
                >
                  <Text 
                    style={{
                      fontFamily: FontType.bold,
                      color: "white",
                      fontSize: 12,
                    }}
                  >
                    Upgrade
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            )}
            <Modal
              animationType="fade"
              transparent
              visible={modalVisible}
              onRequestClose={() => {
                setModalVisible(!modalVisible);
                navigation.replace("SplashScreen");
              }}
            >
              <WebView source={{ uri: url }}  onNavigationStateChange={(state) => {
                if(state.url.includes("https://api.siapasn.com/api/respons")){
                  navigation.replace("SplashScreen");
                }
              }}/>
            </Modal>
            <Modal
              animationType="fade"
              transparent
              visible={modalPaymentVisible}
              onRequestClose={() => {
                setModalPaymentVisible(!modalPaymentVisible);
              }}
            >
              <TouchableOpacity
                onPress={() => setModalPaymentVisible(false)}
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "rgba(0,0,0,0.1)"
                }}
              >
                <View
                  style={{
                    backgroundColor: "white",
                    marginHorizontal: 20,
                    borderRadius: 15
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setModalPaymentVisible(false)}
                    style={{ position: "absolute", right: 10, top: -10 }}
                  >
                    <Image
                      style={{
                        height: 32,
                        width: 32
                      }}
                      resizeMode="contain"
                      source={IMAGES.exit}
                    />
                  </TouchableOpacity>
                  <Image
                    style={{
                      width: width(96),
                      height: height(65),
                      alignSelf: "center",
                      marginTop: 50
                    }}
                    resizeMode="contain"
                    source={IMAGES.poster}
                  />
                  <View style={{flexDirection:'row',padding:20}}>
                    <View style={{flex:1}}>
                      <Text
                        style={{
                          fontSize: 13,
                          fontFamily: FontType.bold,
                          color: "#0B7BF9",
                          textAlign:'right'
                        }}
                      >
                        Gabung Hemat
                      </Text>
                      <Text
                        style={{
                          fontSize: 13,
                          fontFamily: FontType.bold,
                          color: "#0B7BF9",
                          textAlign:'right',
                          textDecorationLine:'line-through'
                        }}
                      >
                        {listPaket.length>1&&_formatRupiah(listPaket[0].Normal_Price, 'Rp. ')}
                      </Text>
                      <TouchableOpacity
                        onPress={()=>_handleBuyPaket(listPaket[0].Name)}
                        style={{
                          backgroundColor: "#0B7BF9",
                          paddingHorizontal: 23,
                          paddingVertical: 11,
                          alignItems: "center",
                          marginTop: 10,
                          borderRadius: 4,
                          alignSelf: "flex-end",
                          marginBottom: 21
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 12,
                            fontFamily: FontType.bold,
                            color: "white",
                            textAlign: "center",
                            alignSelf: "center"
                          }}
                        >
                          {listPaket.length>1&&_formatRupiah(listPaket[0].Discount_Price, 'Rp. ')}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{width:width(15)}}/>
                    <View style={{flex:1}}>
                      <Text
                        style={{
                          fontSize: 13,
                          fontFamily: FontType.bold,
                          color: "#0B7BF9",
                        }}
                      >
                        Gabung Premium
                      </Text>
                      <Text
                        style={{
                          fontSize: 13,
                          fontFamily: FontType.bold,
                          color: "#0B7BF9",
                          textDecorationLine:'line-through'
                        }}
                      >
                        {listPaket.length>2&&_formatRupiah(listPaket[1].Normal_Price, 'Rp. ')}
                      </Text>
                      <TouchableOpacity
                        onPress={()=>_handleBuyPaket(listPaket[1].Name)}
                        style={{
                          backgroundColor: "#0B7BF9",
                          paddingHorizontal: 23,
                          paddingVertical: 11,
                          alignItems: "center",
                          marginTop: 10,
                          borderRadius: 4,
                          alignSelf: "flex-start",
                          marginBottom: 21
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 12,
                            fontFamily: FontType.bold,
                            color: "white",
                            textAlign: "center",
                            alignSelf: "center"
                          }}
                        >
                          {listPaket.length>2&&_formatRupiah(listPaket[1].Discount_Price, 'Rp. ')}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </Modal>
          </View>
          <View style={{height:100}}/>
        </ScrollView>
        )}
      </View>
    </SafeAreaView>
  );
};
export default Home;
