import React, { useEffect, useState } from "react";
import {
  Image,
  ImageBackground,
  Linking,
  Modal,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import styles from "./styles";
import { ENV, IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { Reducers } from "../../redux/types";
import { logout } from "../../redux/actions";
import moment from "moment";

const Home = () => {
  const login = useSelector((state: Reducers) => state.login);
  const [durasi, setDurasi] = useState("");
  const navigation = useNavigation();
  const dispatch = useDispatch();
  useEffect(() => {
    var a = moment(new Date());//now
    var b = moment(login.user.Durasi);
    setDurasi(b.diff(a, 'days'))
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.body}>
            <View style={{ alignItems: "center" }}>
              <View
                style={{
                  width: 80,
                  height: 80,
                  borderRadius: 80,
                  backgroundColor: "red",
                  marginTop:20,
                }}
              />
              <Text
                style={{
                  fontFamily: FontType.bold,
                  color: "#152C07",
                  fontSize: 24,
                  marginTop: 8
                }}
              >
                {login.user.Name}
              </Text>
            </View>
            <ImageBackground
              source={IMAGES.bgProfil}
              style={{
                height: 100,
                width: width(93),
                borderRadius: 10,
                marginBottom: 100,
                marginTop: 24,
                alignItems: "center",
                flexDirection: "row",
                justifyContent: "space-between",
                paddingHorizontal: 18
              }}
            >
              <View>
                <Text
                  style={{
                    color: "#5D5C5D",
                    fontFamily: FontType.regular,
                    fontSize: 16
                  }}
                >
                  Peringkatmu saat ini:
                </Text>
                <Text
                  style={{
                    color: "#5D5C5D",
                    fontFamily: FontType.regular,
                    fontSize: 9
                  }}
                >
                  *nasional
                </Text>
              </View>
              <View>
                <Text
                  style={{
                    color: "#5D5C5D",
                    fontFamily: FontType.bold,
                    fontSize: 28
                  }}
                >
                  1.255
                </Text>
                <Text
                  style={{
                    color: "#5D5C5D",
                    fontFamily: FontType.regular,
                    fontSize: 10
                  }}
                >
                  dari 100.000 peserta
                </Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  bottom: -55,
                  alignItems: "center",
                  width: width(93)
                }}
              >
                <View
                  style={{
                    width: width(70),
                    backgroundColor: "white",
                    borderRadius: 16,
                    flexDirection: "row",
                    justifyContent: "space-around",
                    paddingVertical: 15
                  }}
                >
                  <View style={{ alignItems: "center" }}>
                    <Text
                      style={{
                        color: "#0B7BF9",
                        fontFamily: FontType.SemiBold,
                        fontSize: 9
                      }}
                    >
                      Paket Kamu:
                    </Text>
                    <Text
                      style={{
                        color: "#00CB6A",
                        fontFamily: FontType.bold,
                        fontSize: 12,
                        backgroundColor: "#E0FFE5",
                        borderRadius: 12,
                        paddingHorizontal: 8,
                        paddingVertical: 4,
                        marginTop: 4
                      }}
                    >
                      {login.user.level.Name}
                    </Text>
                  </View>
                  <View
                    style={{ width: 1, height: 35, backgroundColor: "#0B7BF9" }}
                  />
                  <View style={{ alignItems: "center" }}>
                    <Text
                      style={{
                        color: "#0B7BF9",
                        fontFamily: FontType.SemiBold,
                        fontSize: 9
                      }}
                    >
                      Sisa hari:
                    </Text>
                    <Text
                      style={{
                        color: "#0B7BF9",
                        fontFamily: FontType.bold,
                        fontSize: 24
                      }}
                    >
                      {login.user.Durasi ? durasi:'∞'}
                    </Text>
                  </View>
                </View>
              </View>
            </ImageBackground>
            <View style={{ width: width(93) }}>
              <Text
                style={{
                  color: "#152C07",
                  fontFamily: FontType.SemiBold,
                  fontSize: 16
                }}
              >
                Atur Formasi
              </Text>
            </View>
            <View style={{ width: width(93) }}>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  padding: 12,
                  height: 44,
                  borderRadius: 10,
                  marginTop: 16,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: width(60)
                  }}
                >
                  <Text
                    style={{
                      color: "#5D5C5D",
                      fontFamily: FontType.regular,
                      fontSize: 14
                    }}
                  >
                    Pilih Provinsi
                  </Text>
                </View>
                <Image
                  style={{
                    height: 10,
                    width: 10
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                />
              </View>
            </View>
            <TouchableOpacity style={{ width: width(93) }}>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  padding: 12,
                  height: 44,
                  borderRadius: 10,
                  marginTop: 16,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: width(60)
                  }}
                >
                  <Text
                    style={{
                      color: "#5D5C5D",
                      fontFamily: FontType.regular,
                      fontSize: 14
                    }}
                  >
                    Pilih Kota
                  </Text>
                </View>
                <Image
                  style={{
                    height: 10,
                    width: 10
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ width: width(93) }}>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  padding: 12,
                  height: 44,
                  borderRadius: 10,
                  marginTop: 16,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: width(60)
                  }}
                >
                  <Text
                    style={{
                      color: "#5D5C5D",
                      fontFamily: FontType.regular,
                      fontSize: 14
                    }}
                  >
                    Pilih formasi tujuan
                  </Text>
                </View>
                <Image
                  style={{
                    height: 10,
                    width: 10
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ width: width(93) }} onPress={()=>{dispatch(logout(navigation))}}>
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  padding: 12,
                  height: 76,
                  borderRadius: 10,
                  marginTop: 16,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: width(60)
                  }}
                >
                  <View
                    style={{
                      height: 44,
                      width: 44,
                      backgroundColor: "#E0FFF0",
                      borderRadius: 10,
                      marginRight: 14
                    }}
                  />
                  <View>
                    <Text
                      style={{
                        color: "#152C07",
                        fontFamily: FontType.bold,
                        fontSize: 16
                      }}
                    >
                      Keluar
                    </Text>
                    <Text
                      style={{
                        color: "#5D5C5D",
                        fontFamily: FontType.regular,
                        fontSize: 12
                      }}
                    >
                      Keluar dari aplikasi
                    </Text>
                  </View>
                </View>
                {/* <Image
                  style={{
                    height: 20,
                    width: 20
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                /> */}
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
export default Home;
