import React, { useState } from "react";
import { ImageBackground, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { IMAGES } from "../../configs";
import { Reducers } from "../../redux/types";

import styles from "./styles";
import { newhost } from "../../configs/api";
import { LOGIN, SAVE_LINK } from "../../redux/actions";

const Home = () => {
  const navigation = useNavigation();
  const login = useSelector((state: Reducers) => state.login);
  const dispatch = useDispatch();
  const handleSplashScreen = async () => {
    !login.jwttoken ? navigation.replace("HomeLogin") : _handleCheckUser();
  };
  const _handleCheckUser = () => {
    const data = login.user;
    if (data.Phone_number || data.Provence || data.City || data.Name) {
      _HandleGetProfil()
      navigation.replace("Home");
    } else {
      navigation.replace("Register");
    }
  };
  const _HandleGetData = () => {
    newhost.get("api/links").then(res => {
      dispatch({
        type: SAVE_LINK,
        payload: {
          data: res.data.data[res.data.data.length-1]
        }
      });
    });
  };
  const _handleCheckPaket = async () => {
    newhost.get("api/check-ownership", {
      headers: {
        Authorization: `Bearer ${login.jwttoken}`,
      },
    })
  }
  const _handleUpgradePaket = async () => {
    newhost.get("api/ownerships", {
      headers: {
        Authorization: `Bearer ${login.jwttoken}`,
      },
    })
  }
  const _HandleGetProfil = async () => {
    await _handleUpgradePaket();
    await _handleCheckPaket();
    newhost.get("api/users/me", {
      headers: {
        Authorization: `Bearer ${login.jwttoken}`,
      },
    }).then(res => {
      dispatch({
        type: LOGIN,
        payload: {
          data: {
            jwtToken: login.jwttoken,
            user: res.data
          }
        }
      });
    });
  };
  React.useEffect(() => {
    _HandleGetData();
    setTimeout(() => {
      handleSplashScreen();
    }, 1000);
  }, []);
  return (
    <View style={styles.container}>
      <ImageBackground style={styles.bodyhome} source={IMAGES.splash} />
    </View>
  );
};
export default Home;
