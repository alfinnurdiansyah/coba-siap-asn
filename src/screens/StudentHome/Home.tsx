import React, { useEffect, useState } from "react";
import {
  Image,
  Linking,
  Modal,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  ImageBackground,
  View,
  PermissionsAndroid,
  Alert
} from "react-native";
import { useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import RBSheet from "react-native-raw-bottom-sheet";
import Contacts from "react-native-contacts";
import styles from "./styles";
import { ENV, IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { Reducers } from "../../redux/types";
import { newhost } from "../../configs/api";

const Home = () => {
  // const refRBSheet = useRef();
  const login = useSelector((state: Reducers) => state.login);
  const [page, setPage] = useState("gratis");
  const [listBanner, setListBanner] = useState([]);
  const [listPPPK, setListPPPK] = useState([]);
  const [mylistPPPK, setmyListPPPK] = useState([]);
  const [listTeknis, setListTeknis] = useState([]);
  const [mylistTeknis, setmyListTeknis] = useState([]);
  const [modalExam, setModalExam] = useState(false);
  const [modalLatihan, setModalLatihan] = useState(false);
  const [listExam, setListExam] = useState([]);
  const [NameTryout, setNameTryout] = useState("");
  const [katakata, setKataKata] = useState();
  const navigation = useNavigation();
  const _HandleGetData = () => {
    newhost.get("api/banners?populate=*").then(res => {
      setListBanner(res.data.data);
    });
  };
  const _HandleGetKataKata = () => {
    newhost.get("api/displays?populate=*").then(res => {
      setKataKata(res.data.data[res.data.data.length-1]);
    });
  };
  const _HandleGetTryoutPPPK = () => {
    newhost
      .get("/api/tryouts/p3k", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        function compare( a:any, b:any ) {
          if ( a.Name < b.Name ){
            return -1;
          }
          if ( a.Name > b.Name ){
            return 1;
          }
          return 0;
        }
        const data =res.data.sort( compare );
        setListPPPK(data);
      });
  };
  const _HandleGetMyTryoutPPPK = () => {
    newhost
      .get("/api/tryouts/list", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        setmyListPPPK(res.data);
      });
  };
  const _HandleGetTryoutTeknis = () => {
    newhost
      .get("/api/tryouts/listNoFormation", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        function compare( a:any, b:any ) {
          if ( a.Name < b.Name ){
            return -1;
          }
          if ( a.Name > b.Name ){
            return 1;
          }
          return 0;
        }
        const data =res.data.sort( compare );
        setListTeknis(data);
      });
  };
  const _HandleGetMyTryoutTeknis = () => {
    newhost
      .get("/api/tryouts/UserNoFormation", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${login.jwttoken}`
        }
      })
      .then(res => {
        setmyListTeknis(res.data);
      });
  };
  useEffect(() => {
    _HandleGetKataKata();
    _HandleGetTryoutPPPK();
    _HandleGetMyTryoutPPPK();
    _HandleGetData();
    _HandleGetTryoutTeknis();
    _HandleGetMyTryoutTeknis();
  }, []);
  const _handleOpenTryout = (item: any) => {
    setListExam(item.exams);
    setModalExam(true);
    setNameTryout(item.Name);
  };
  const _handleLoadView = (item: any) => {
    function checkAge(items:any) {
      return items.id === item.id;
    }
    const index =mylistPPPK.findIndex(checkAge);
    if(index!==-1){
      return (<View
        style={{
          backgroundColor: "#FFFFFF",
          padding: 12,
          borderRadius: 10,
          marginTop: 8,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          overflow: "hidden"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: width(60)
          }}
        >
          <View
            style={{
              height: 20,
              width: 20,
              backgroundColor: "blue",
              borderRadius: 6,
              marginRight: 14
            }}
          />
          <View>
            <Text
              style={{
                color: "#152C07",
                fontFamily: FontType.bold,
                fontSize: 18
              }}
            >
              {item.Name}
            </Text>
            <Text
              style={{
                color: "#5D5C5D",
                fontSize: 12,
                fontFamily: FontType.regular
              }}
            >
              {item.Description}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => _handleOpenTryout(mylistPPPK[index])}
          style={{
            backgroundColor: "#0B7BF9",
            paddingVertical: 4,
            paddingHorizontal: 10,
            borderRadius: 10
          }}
        >
          <Text style={{ color: "white", textAlign: "center" }}>
            Kerjakan
          </Text>
        </TouchableOpacity>
      </View>)
    }
    else{
      return(<View
        style={{
          backgroundColor: "#FFFFFF",
          padding: 12,
          borderRadius: 10,
          marginTop: 8,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          overflow: "hidden"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: width(55)
          }}
        >
          <View
            style={{
              height: 20,
              width: 20,
              backgroundColor: "blue",
              borderRadius: 6,
              marginRight: 14
            }}
          />
          <View>
            <Text
              style={{
                color: "#152C07",
                fontFamily: FontType.bold,
                fontSize: 18
              }}
            >
              {item.Name}
            </Text>
            <Text
              style={{
                color: "#5D5C5D",
                fontSize: 12,
                fontFamily: FontType.regular
              }}
            >
              {item.Description}
            </Text>
          </View>
        </View>
        <Image
                  style={{
                    height: 20,
                    width: 20,
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                />
        {/* <View style={{ marginRight: 10 }}>
          <Text
            style={{
              textDecorationLine: "line-through",
              textDecorationStyle: "solid",
              color: "#BEBEBE",
              fontSize: 12,
              fontFamily: FontType.bold
            }}
          >
            Rp
            {item.Price_normal}
          </Text>
          <Text
            style={{
              color: "#0B7BF9",
              fontSize: 14,
              fontFamily: FontType.bold
            }}
          >
            Rp
            {item.Price_discount}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "#F5C114",
            paddingVertical: 4,
            paddingHorizontal: 10,
            borderRadius: 10
          }}
        >
          <Text style={{ color: "white", textAlign: "center" }}>
            Beli
          </Text>
        </View> */}
      </View>)
    }             
  };
  const _handleLoadViewTeknis = (item: any) => {
    function checkAge(items:any) {
      return items.id === item.id;
    }
    const index =mylistTeknis.findIndex(checkAge);
    if(index!==-1){
      return (<View
        style={{
          backgroundColor: "#FFFFFF",
          padding: 12,
          borderRadius: 10,
          marginTop: 8,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          overflow: "hidden"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: width(60)
          }}
        >
          <View
            style={{
              height: 20,
              width: 20,
              backgroundColor: "blue",
              borderRadius: 6,
              marginRight: 14
            }}
          />
          <View>
            <Text
              style={{
                color: "#152C07",
                fontFamily: FontType.bold,
                fontSize: 18
              }}
            >
              {item.Name}
            </Text>
            <Text
              style={{
                color: "#5D5C5D",
                fontSize: 12,
                fontFamily: FontType.regular
              }}
            >
              {item.Description}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => _handleOpenTryout(mylistPPPK[index])}
          style={{
            backgroundColor: "#0B7BF9",
            paddingVertical: 4,
            paddingHorizontal: 10,
            borderRadius: 10
          }}
        >
          <Text style={{ color: "white", textAlign: "center" }}>
            Kerjakan
          </Text>
        </TouchableOpacity>
      </View>)
    }
    else{
      return(<View
        style={{
          backgroundColor: "#FFFFFF",
          padding: 12,
          borderRadius: 10,
          marginTop: 8,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          overflow: "hidden"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: width(55)
          }}
        >
          <View
            style={{
              height: 20,
              width: 20,
              backgroundColor: "blue",
              borderRadius: 6,
              marginRight: 14
            }}
          />
          <View>
            <Text
              style={{
                color: "#152C07",
                fontFamily: FontType.bold,
                fontSize: 18
              }}
            >
              {item.Name}
            </Text>
            <Text
              style={{
                color: "#5D5C5D",
                fontSize: 12,
                fontFamily: FontType.regular
              }}
            >
              {item.Description}
            </Text>
          </View>
        </View>
        <Image
                  style={{
                    height: 20,
                    width: 20,
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                />
        {/* <View style={{ marginRight: 10 }}>
          <Text
            style={{
              textDecorationLine: "line-through",
              textDecorationStyle: "solid",
              color: "#BEBEBE",
              fontSize: 12,
              fontFamily: FontType.bold
            }}
          >
            Rp
            {item.Price_normal}
          </Text>
          <Text
            style={{
              color: "#0B7BF9",
              fontSize: 14,
              fontFamily: FontType.bold
            }}
          >
            Rp
            {item.Price_discount}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "#F5C114",
            paddingVertical: 4,
            paddingHorizontal: 10,
            borderRadius: 10
          }}
        >
          <Text style={{ color: "white", textAlign: "center" }}>
            Beli
          </Text>
        </View> */}
      </View>)
    }             
  };
  const _handleOpenExam = (item: any) => {
    setModalExam(false);
    navigation.navigate("Tryout", { NameTryout, NameExam: item.Name });
  };
  const _handleSaveContact = () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
      title: "Contacts",
      message: "This app would like to view your contacts.",
      buttonPositive: "Please accept bare mortal"
    }).then(() => {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts.",
        buttonPositive: "Please accept bare mortal"
      }).then(() => {
        const newPerson = {
          familyName: "Safir Academy",
          givenName: "",
          phoneNumbers: [
            {
              label: "mobile",
              number: login.link.attributes.Kontak_wa
            }
          ]
        };
        Contacts.addContact(newPerson);
        Alert.alert("Berhasil","Kontak Telah Berhasil Disimpan")
      });
    });
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.body}>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                width: width(93),
                alignItems: "center",
                marginVertical: 15
              }}
            >
              <Image style={styles.logo} source={IMAGES.siapasn} />
              <TouchableOpacity
                style={{ flexDirection: "row" }} 
                onPress={()=>navigation.navigate("Profil")}
              >
                <View style={{ alignItems: "flex-end", marginRight: 10 }}>
                  <Text
                    style={{
                      color: "#BEBEBE",
                      fontSize: 12,
                      fontFamily: FontType.SemiBold
                    }}
                  >
                    Halo,
                  </Text>
                  <Text
                    style={{
                      fontFamily: FontType.bold,
                      color: "#5D5C5D",
                      fontSize: 20,
                      maxWidth:width(40),
                    }}
                    numberOfLines={1}
                  >
                    {login.user.Name}
                  </Text>
                </View>
                <View
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: 40,
                    backgroundColor: "red"
                  }}
                />
              </TouchableOpacity>
            </View>
            <ScrollView
              style={{
                width: width(93)
              }}
              pagingEnabled
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              {listBanner.map(res => (
                <TouchableOpacity
                  style={{
                    width: width(93),
                    height: 195,
                    backgroundColor: "#FAC40A",
                    borderRadius: 10,
                    overflow:'hidden'
                  }}
                  onPress={async () =>
                    await Linking.openURL(res.attributes.Link)
                  }
                >
                  <Image
                    style={{ flex: 1 }}
                    source={{
                      uri:
                        ENV.newHost + res.attributes.Image.data.attributes.url
                    }}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
            <ScrollView
              style={{
                width: width(100),
                marginTop: 18
              }}
              pagingEnabled
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              <View style={{ width: width(3.5) }} />
              <TouchableOpacity
                onPress={() => setPage("gratis")}
                style={{
                  backgroundColor: page === "gratis" ? "#FAC40A" : "#E2E0E0",
                  borderRadius: 8,
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  marginRight: 8
                }}
              >
                <Text
                  style={{
                    fontFamily: FontType.bold,
                    fontSize: 12,
                    color: page === "gratis" ? "#5D5C5D" : "#BEBEBE"
                  }}
                >
                  GRATIS
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPage("paketPPPK")}
                style={{
                  backgroundColor: page === "paketPPPK" ? "#FAC40A" : "#E2E0E0",
                  borderRadius: 8,
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  marginRight: 8
                }}
              >
                <Text
                  style={{
                    fontFamily: FontType.bold,
                    fontSize: 12,
                    color: page === "paketPPPK" ? "#5D5C5D" : "#BEBEBE"
                  }}
                >
                  PAKET PPPK
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPage("kompetensi")}
                style={{
                  backgroundColor:
                    page === "kompetensi" ? "#FAC40A" : "#E2E0E0",
                  borderRadius: 8,
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  marginRight: 8
                }}
              >
                <Text
                  style={{
                    fontFamily: FontType.bold,
                    fontSize: 12,
                    color: page === "kompetensi" ? "#5D5C5D" : "#BEBEBE"
                  }}
                >
                  KOMPETENSI TEKNIS
                </Text>
              </TouchableOpacity>
            </ScrollView>
            {page === "gratis" && (
              <View style={{ width: width(93) }}>
                <Text
                  style={{
                    marginTop: 18,
                    color: "#EFBE24",
                    fontFamily: FontType.ExtraBold,
                    fontSize: 14
                  }}
                >
                  TRY OUT NASIONAL
                </Text>
                <ImageBackground
                  source={{uri:katakata&&ENV.newHost + katakata.attributes.Image_TO.data.attributes.url}}
                  style={{
                    backgroundColor: "#F5C114",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    overflow: "hidden"
                  }}
                  resizeMode="stretch"
                >
                  <View>
                    <Text
                      style={{
                        color: "white",
                        fontFamily: FontType.bold,
                        fontSize: 18
                      }}
                    >
                      COMING SOON
                    </Text>
                    <Text
                      style={{
                        marginTop: 8,
                        fontFamily: FontType.regular,
                        fontSize: 16,
                        color: "rgba(93, 92, 93, 1)"
                      }}
                    >
                      17 juni 2022
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: "#0B7BF9",
                      paddingVertical: 4,
                      paddingHorizontal: 10,
                      borderRadius: 8
                    }}
                  >
                    <Text style={{ color: "white" }}>Daftar</Text>
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      right: -25,
                      top: 0,
                      alignItems: "center",
                      width: 100,
                      transform: [{ rotate: "45deg" }],
                      backgroundColor: "#00CB6A"
                    }}
                  >
                    <Text style={{ color: "white", marginLeft: 25 }}>
                      Gratis
                    </Text>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    marginTop: 18,
                    color: "#EFBE24",
                    fontFamily: FontType.ExtraBold,
                    fontSize: 14
                  }}
                >
                  TRY OUT SIMULASI
                </Text>
                <View
                  style={{
                    backgroundColor: "rgba(245, 193, 20, 0.8)",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    overflow: "hidden"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        color: "white",
                        fontFamily: FontType.bold,
                        fontSize: 18
                      }}
                    >
                      Simulasi
                    </Text>
                    <Text
                      style={{
                        marginTop: 8,
                        fontFamily: FontType.regular,
                        fontSize: 16,
                        color: "rgba(93, 92, 93, 1)"
                      }}
                    >
                      Ayo uji kesiapanmu
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: "#0B7BF9",
                      paddingVertical: 4,
                      paddingHorizontal: 10,
                      borderRadius: 8
                    }}
                  >
                    <Text style={{ color: "white" }}>Kerjakan</Text>
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      right: -25,
                      top: 0,
                      alignItems: "center",
                      width: 100,
                      transform: [{ rotate: "45deg" }],
                      backgroundColor: "#00CB6A"
                    }}
                  >
                    <Text style={{ color: "white", marginLeft: 25 }}>
                      Gratis
                    </Text>
                  </View>
                </View>
                <Text
                  style={{
                    marginTop: 18,
                    fontFamily: FontType.ExtraBold,
                    fontSize: 14,
                    color: "#EFBE24"
                  }}
                >
                  BONUS LATIHAN
                </Text>
                <TouchableOpacity
                  onPress={()=>setModalLatihan(true)}
                  style={{
                    backgroundColor: "rgba(245, 193, 20, 0.6)",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    overflow: "hidden"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        color: "white",
                        fontFamily: FontType.bold,
                        fontSize: 18
                      }}
                    >
                      Bonus latihan
                    </Text>
                    <Text
                      style={{
                        marginTop: 8,
                        fontFamily: FontType.regular,
                        fontSize: 16,
                        color: "rgba(93, 92, 93, 1)"
                      }}
                    >
                      Yuk belajar setiap hari
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: "#0B7BF9",
                      paddingVertical: 4,
                      paddingHorizontal: 10,
                      borderRadius: 8
                    }}
                  >
                    <Text style={{ color: "white" }}>Buka</Text>
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      right: -25,
                      top: 0,
                      alignItems: "center",
                      width: 100,
                      transform: [{ rotate: "45deg" }],
                      backgroundColor: "#00CB6A"
                    }}
                  >
                    <Text style={{ color: "white", marginLeft: 25 }}>
                      Gratis
                    </Text>
                  </View>
                </TouchableOpacity>
                {/* <Text
                  style={{
                    marginTop: 18,
                    color: "#EFBE24",
                    fontFamily: FontType.ExtraBold,
                    fontSize: 14
                  }}
                >
                  LAYANAN CPNS
                </Text>
                <View
                  style={{
                    backgroundColor: "rgba(245, 193, 20, 0.6)",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    overflow: "hidden"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        color: "white",
                        fontFamily: FontType.bold,
                        fontSize: 18
                      }}
                    >
                      grup belajar cpns
                    </Text>
                    <Text style={{ marginTop: 8 }}>
                      Bagi anda yang ingin mendalami tes CPNS
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: "#0B7BF9",
                      paddingVertical: 4,
                      paddingHorizontal: 10,
                      borderRadius: 8
                    }}
                  >
                    <Text style={{ color: "white" }}>Buka</Text>
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      right: -25,
                      top: 0,
                      alignItems: "center",
                      width: 100,
                      transform: [{ rotate: "45deg" }],
                      backgroundColor: "#00CB6A"
                    }}
                  >
                    <Text style={{ color: "white", marginLeft: 25 }}>
                      Gratis
                    </Text>
                  </View>
                </View> */}
              </View>
            )}
            {page === "paketPPPK" && (
              <View style={{ width: width(93) }}>
                <Text
                  style={{
                    textAlign: "center",
                    color: "#152C07",
                    fontFamily: FontType.bold,
                    fontSize: 18,
                    marginTop: 21
                  }}
                >
                  {katakata&&katakata.attributes.Letter}
                </Text>
                <Text
                  style={{
                    marginTop: 18,
                    color: "#EFBE24",
                    fontFamily: FontType.ExtraBold,
                    fontSize: 14
                  }}
                >
                  PAKET PENDAMPINGAN
                </Text>
                <View
                  style={{
                    backgroundColor: "#F5C114",
                    padding: 12,
                    borderRadius: 10,
                    marginTop: 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    overflow: "hidden"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        color: "white",
                        fontFamily: FontType.bold,
                        fontSize: 14
                      }}
                    >
                      PAKET PPPK
                    </Text>
                    <Text
                      style={{
                        marginTop: 4,
                        color: "white",
                        fontFamily: FontType.regular,
                        fontSize: 14
                      }}
                    >
                      Gabung untuk lebih hemat
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={()=>navigation.navigate("Paket")}
                    style={{
                      backgroundColor: "#0B7BF9",
                      paddingVertical: 4,
                      paddingHorizontal: 12,
                      borderRadius: 8
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        textAlign: "center",
                        fontSize: 12,
                        fontFamily: FontType.bold
                      }}
                    >
                      Lihat
                      {"\n"}
                      Paket
                    </Text>
                  </TouchableOpacity>
                </View>
                <Text
                  style={{
                    marginTop: 18,
                    color: "#EFBE24",
                    fontFamily: FontType.ExtraBold,
                    fontSize: 14
                  }}
                >
                  TRY OUT satuan
                </Text>
                {listPPPK.map(item => _handleLoadView(item))}
              </View>
            )}
            {page === "kompetensi" && (
              <View style={{ width: width(93) }}>
                <Text
                  style={{
                    textAlign: "center",
                    color: "#152C07",
                    fontFamily: FontType.bold,
                    fontSize: 18,
                    marginTop: 21
                  }}
                >
                  {katakata&&katakata.attributes.Letter}
                </Text>
                {listTeknis.map(item => _handleLoadViewTeknis(item))}
              </View>
            )}
            {/* <Button title="OPEN BOTTOM SHEET" onPress={() => refRBSheet.current.open()} /> */}
            {/* <Button
              title="LOG OUT"
              onPress={() => dispatch(logout(navigation, ""))}
            /> */}
          </View>
        </ScrollView>
      </View>
      <Modal
        animationType="fade"
        transparent
        visible={modalExam}
        onRequestClose={() => {
          setModalExam(!modalExam);
        }}
      >
        <TouchableOpacity
          onPress={() => setModalExam(false)}
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,0.1)"
          }}
        >
          <View
            style={{
              backgroundColor: "#F5C114",
              marginHorizontal: 20,
              paddingHorizontal: 10,
              borderRadius: 15,
              paddingVertical: 20,
              paddingTop: 50
            }}
          >
            <TouchableOpacity
              onPress={() => setModalExam(false)}
              style={{ position: "absolute", right: 5, top: 5 }}
            >
              <Image
                style={{
                  height: 40,
                  width: 40
                }}
                resizeMode="contain"
                source={IMAGES.exitBlue}
              />
            </TouchableOpacity>
            {listExam.map(item => (
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  padding: 12,
                  borderRadius: 10,
                  marginTop: 8,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: width(60)
                  }}
                >
                  <View>
                    <Text
                      style={{
                        color: "#152C07",
                        fontFamily: FontType.bold,
                        fontSize: 18
                      }}
                    >
                      {item.Name}
                    </Text>
                    <Text
                      style={{
                        color: "#5D5C5D",
                        fontSize: 12,
                        fontFamily: FontType.regular
                      }}
                    >
                      {item.Deskripsi_exam}
                    </Text>
                  </View>
                </View>
                <View style={{marginRight:6,paddingHorizontal:4,paddingVertical:2,borderRadius:8,backgroundColor:item.Akses < 1?"#F26666":"#F5C114"}}>
                  <Text
                      style={{
                        color: "white",
                        fontSize: 12,
                        fontFamily: FontType.ExtraBold
                      }}
                    >
                      {item.Akses+"x"}
                  </Text>
                </View>
                <TouchableOpacity
                  disabled={item.Akses < 1}
                  onPress={() => _handleOpenExam(item)}
                  style={{
                    backgroundColor:item.Akses <1? "#BEBEBE" :"#0B7BF9",
                    paddingVertical: 4,
                    paddingHorizontal: 10,
                    borderRadius: 10
                  }}
                >
                  <Text style={{ color: "white", textAlign: "center" }}>
                    Kerjakan
                  </Text>
                </TouchableOpacity>
              </View>
            ))}
          </View>
        </TouchableOpacity>
      </Modal>
      <Modal
        animationType="fade"
        transparent
        visible={modalLatihan}
        onRequestClose={() => {
          setModalLatihan(!modalLatihan);
        }}
      >
        <TouchableOpacity
          onPress={() => setModalLatihan(false)}
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,0.1)"
          }}
        >
          <View
            style={{
              backgroundColor: "#FFFFFF",
              marginHorizontal: 20,
              paddingHorizontal: 10,
              borderRadius: 15,
              paddingVertical: 20,
              paddingTop: 50
            }}
          >
            <TouchableOpacity
              onPress={() => setModalLatihan(false)}
              style={{ position: "absolute", right: 5, top: 5 }}
            >
              <Image
                style={{
                  height: 40,
                  width: 40
                }}
                resizeMode="contain"
                source={IMAGES.exitBlue}
              />
            </TouchableOpacity>
            <View style={{width:width(90),paddingHorizontal:20}}>
              <Text style={{ color: "#5D5C5D",fontSize:16 }}>
                Hai kak, sudah siap belajar? 😉
              </Text>
              <Text style={{ color: "#0D7CF9",fontSize:16,marginTop:20 }}>
                LATIHAN SOAL HARIAN
              </Text>
              <Text style={{ color: "#5D5C5D",fontSize:16 }}>
                Ini merupakan layanan SiapASN GRATIS berupa:
              </Text>
              <Text style={{ color: "#5D5C5D",fontSize:16,marginTop:20 }}>
                1. Latihan 1-2 soal setiap hari sampai hari H seleksi{'\n'}
                2. Info ter Update mengenai PPPK 2022
              </Text>
              <Text style={{ color: "#5D5C5D",fontSize:16,marginTop:20 }}>
                Silahkan memilih metode latihan soal harian
              </Text>
              <View style={{flexDirection:'row',marginTop:20  }}>
                <TouchableOpacity onPress={()=>_handleSaveContact()} style={{flex:1,alignItems:'center',backgroundColor:'#00CB6A',borderRadius:4,paddingVertical:10}}>
                  <Text style={{ color: "#FFFFFF",fontSize:14,fontFamily: FontType.ExtraBold, }}>
                    Status WA
                  </Text>
                </TouchableOpacity>
                <View style={{width:25}}/>
                <TouchableOpacity style={{flex:1,alignItems:'center',backgroundColor:'#0B7BF9',borderRadius:4,paddingVertical:10}}>
                  <Text style={{ color: "#FFFFFF",fontSize:14,fontFamily: FontType.ExtraBold, }}>
                    Grup Telegram
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
      <RBSheet
        height={200}
        // ref={refRBSheet}
        closeOnDragDown
        closeOnPressMask={false}
        customStyles={{
          wrapper: {
            backgroundColor: "transparent"
          },
          draggableIcon: {
            backgroundColor: "#000"
          }
        }}
      />
    </SafeAreaView>
  );
};
export default Home;
