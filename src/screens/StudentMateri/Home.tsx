import React, { useEffect, useState } from "react";
import {
  Image,
  Linking,
  Modal,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import RBSheet from "react-native-raw-bottom-sheet";

import { ENV, IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { Reducers } from "../../redux/types";
import { newhost } from "../../configs/api";

import styles from "./styles";

const Home = () => {
  const login = useSelector((state: Reducers) => state.login);
  const [listBanner, setListBanner] = useState([]);
  const [listMateri, setListMateri] = useState([]);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(false);
  const _HandleGetData = () => {
    newhost.get("api/banners?populate=*").then((res) => {
      setListBanner(res.data.data);
    });
  };
  const _HandleCheckAccess = (data:any) => {
    navigation.navigate("SubMateri",{title:data.Name,material:data.materials})
  };
  const _HandleGetDataMateri = () => {
    newhost
      .get("api/type-materials", {
        headers: {
          Authorization: `Bearer ${login.jwttoken}`,
        },
      })
      .then((res) => {
        setListMateri(res.data);
      });
  };

  useEffect(() => {
    _HandleGetData();
    _HandleGetDataMateri();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.body}>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                width: width(93),
                alignItems: "center",
                marginVertical: 15,
              }}
            >
              <Image style={styles.logo} source={IMAGES.siapasn} />
              <TouchableOpacity style={{ flexDirection: "row" }} onPress={()=>navigation.navigate("Profil")}>
                <View style={{ alignItems: "flex-end", marginRight: 10 }}>
                  <Text
                    style={{
                      color: "#BEBEBE",
                      fontSize: 12,
                      fontFamily: FontType.SemiBold,
                    }}
                  >
                    Halo,
                  </Text>
                  <Text
                    style={{
                      fontFamily: FontType.bold,
                      color: "#5D5C5D",
                      fontSize: 20,
                      maxWidth:width(40),
                    }}
                    numberOfLines={1}
                  >
                    {login.user.Name}
                  </Text>
                </View>
                <View
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: 40,
                    backgroundColor: "red",
                  }}
                />
              </TouchableOpacity>
            </View>
            <ScrollView
              style={{
                width: width(93),
              }}
              pagingEnabled
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              {listBanner.map(res => (
                <TouchableOpacity
                  style={{
                    width: width(93),
                    height: 195,
                    backgroundColor: "#FAC40A",
                    borderRadius: 10,
                    overflow:'hidden'
                  }}
                  onPress={async () =>
                    await Linking.openURL(res.attributes.Link)
                  }
                >
                  <Image
                    style={{ flex: 1 }}
                    source={{
                      uri:
                        ENV.newHost + res.attributes.Image.data.attributes.url
                    }}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
            {listMateri.map((res) => (
              <TouchableOpacity
                style={{ width: width(93) }}
                onPress={() => _HandleCheckAccess(res)}
              >
                <View
                  style={{
                    backgroundColor: "#FFFFFF",
                    padding: 12,
                    height: 76,
                    borderRadius: 10,
                    marginTop: 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    overflow: "hidden",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      width: width(60),
                    }}
                  >
                    {res.Media ? (
                      <Image
                        style={{
                          height: 33,
                          width: 33,
                          backgroundColor: "#E0FFF0",
                          borderRadius: 10,
                          marginRight: 14,
                        }}
                        source={{
                          uri: ENV.newHost + res.Media[0].url,
                        }}
                      />
                    ) : (
                      <View
                        style={{
                          height: 33,
                          width: 33,
                          backgroundColor: "#E0FFF0",
                          borderRadius: 10,
                          marginRight: 14,
                        }}
                      />
                    )}
                    <Text
                      style={{
                        color: "#152C07",
                        fontFamily: FontType.bold,
                        fontSize: 18,
                      }}
                    >
                      {res.Name}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            ))}
            <Modal
              animationType="fade"
              transparent
              visible={modalVisible}
              onRequestClose={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <TouchableOpacity
                onPress={() => setModalVisible(false)}
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "rgba(0,0,0,0.1)",
                }}
              >
                <View
                  style={{
                    backgroundColor: "white",
                    marginHorizontal: 20,
                    paddingHorizontal: 30,
                    borderRadius: 15,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setModalVisible(false)}
                    style={{ position: "absolute", right: 25, top: 25 }}
                  >
                    <Image
                      style={{
                        height: 32,
                        width: 32,
                      }}
                      resizeMode="contain"
                      source={IMAGES.exit}
                    />
                  </TouchableOpacity>
                  <Image
                    style={{
                      height: 60,
                      width: 60,
                      alignSelf: "center",
                      marginTop: 50,
                    }}
                    resizeMode="contain"
                    source={IMAGES.info}
                  />
                  <Text
                    style={{
                      color: "#5D5C5D",
                      fontFamily: FontType.regular,
                      fontSize: 16,
                      textAlign: "center",
                    }}
                  >
                    Kamu belum dapat mengakses fitur ini. silakan melakukan
                    pembelian paket terlebih dahulu untuk membuka keseluruhan
                    materi
                  </Text>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "#F5C114",
                      paddingHorizontal: 23,
                      paddingVertical: 11,
                      alignItems: "center",
                      marginTop: 24,
                      borderRadius: 4,
                      alignSelf: "center",
                      marginBottom: 21,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: FontType.SemiBold,
                        color: "white",
                        textAlign: "center",
                        alignSelf: "center",
                      }}
                    >
                      INFO PAKET
                    </Text>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </Modal>
          </View>
        </ScrollView>
      </View>
      <RBSheet
        height={200}
        // ref={refRBSheet}
        closeOnDragDown
        closeOnPressMask={false}
        customStyles={{
          wrapper: {
            backgroundColor: "transparent",
          },
          draggableIcon: {
            backgroundColor: "#000",
          },
        }}
      />
    </SafeAreaView>
  );
};
export default Home;
