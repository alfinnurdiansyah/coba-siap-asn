import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F7F7"
  },
  body: {
    width: "100%",
    alignItems: "center"
  },
  input: {
    marginTop: 10,
    width: "90%",
    paddingStart: 15,
    borderRadius: 10,
    backgroundColor: "#FFFFFF",
    borderColor: "#808080",
    borderWidth: 0.4
  },
  logo: {
    width: 122,
    height: 40
  },
  katex: {
    flex: 1
  }
});

export default styles;
