import React, { useEffect, useState } from "react";
import {
  Image,
  Modal,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import RBSheet from "react-native-raw-bottom-sheet";
import styles from "./styles";
import { ENV, IMAGES } from "../../configs";
import FontType from "../../assets/fonts";
import width from "../../utils/widthPercent";
import { WebView } from "react-native-webview";
import DeviceInfo from 'react-native-device-info';
const Home = ({route}) => {
  const { title,material } = route.params;
  const [modalVisible, setModalVisible] = useState(false);
  const [modalMateri, setModalMateri] = useState(false);
  const [linkMateri, setLinkMateri] = useState('');
  const _HandleCheckAccess = (data:any) => {
    if(data.Status===0){
      setModalVisible(true)
    }else{
      setLinkMateri(data.Link)
      setModalMateri(true)
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.body}>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                width: width(93),
                alignItems: "center",
                marginVertical: 15
              }}
            >
              <Text
                style={{
                  color: "#152C07",
                  fontFamily: FontType.bold,
                  fontSize: 18
                }}
              >
                {title}
              </Text>
            </View>
            {material.map((res) => (
              <TouchableOpacity
              style={{ width: width(93) }}
              onPress={() => _HandleCheckAccess(res)}
            >
              <View
                style={{
                  backgroundColor: "#FFFFFF",
                  padding: 12,
                  height: 76,
                  borderRadius: 10,
                  marginTop: 8,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  overflow: "hidden",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    width: width(60),
                  }}
                >
                  {res.Media ? (
                    <Image
                      style={{
                        height: 33,
                        width: 33,
                        backgroundColor: "#E0FFF0",
                        borderRadius: 10,
                        marginRight: 14,
                      }}
                      source={{
                        uri: ENV.newHost + res.Media[0].url,
                      }}
                    />
                  ) : (
                    <View
                      style={{
                        height: 33,
                        width: 33,
                        backgroundColor: "#E0FFF0",
                        borderRadius: 10,
                        marginRight: 14,
                      }}
                    />
                  )}
                  <View>
                    <Text
                      style={{
                        color: "#152C07",
                        fontFamily: FontType.bold,
                        fontSize: 18
                      }}
                    >
                      {res.Name}
                    </Text>
                    <Text
                      style={{
                        color: "#5D5C5D",
                        fontSize: 12,
                        fontFamily: FontType.regular
                      }}
                    >
                      {res.Description}
                    </Text>
                  </View>
                </View>
                {res.Status===0&&<Image
                  style={{
                    height: 20,
                    width: 20,
                  }}
                  resizeMode="contain"
                  source={IMAGES.lock}
                />}
              </View>
            </TouchableOpacity>
            ))}
            <Modal
              animationType="fade"
              transparent
              visible={modalVisible}
              onRequestClose={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <TouchableOpacity
                onPress={() => setModalVisible(false)}
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "rgba(0,0,0,0.1)"
                }}
              >
                <View
                  style={{
                    backgroundColor: "white",
                    marginHorizontal: 20,
                    paddingHorizontal: 30,
                    borderRadius: 15
                  }}
                >
                  <TouchableOpacity
                    onPress={() => setModalVisible(false)}
                    style={{ position: "absolute", right: 25, top: 25 }}
                  >
                    <Image
                      style={{
                        height: 32,
                        width: 32
                      }}
                      resizeMode="contain"
                      source={IMAGES.exit}
                    />
                  </TouchableOpacity>
                  <Image
                    style={{
                      height: 60,
                      width: 60,
                      alignSelf: "center",
                      marginTop: 50
                    }}
                    resizeMode="contain"
                    source={IMAGES.info}
                  />
                  <Text
                    style={{
                      color: "#5D5C5D",
                      fontFamily: FontType.regular,
                      fontSize: 16,
                      textAlign: "center"
                    }}
                  >
                    Kamu belum dapat mengakses fitur ini. silakan melakukan
                    pembelian paket terlebih dahulu untuk membuka keseluruhan
                    materi
                  </Text>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "#F5C114",
                      paddingHorizontal: 23,
                      paddingVertical: 11,
                      alignItems: "center",
                      marginTop: 24,
                      borderRadius: 4,
                      alignSelf: "center",
                      marginBottom: 21
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: FontType.SemiBold,
                        color: "white",
                        textAlign: "center",
                        alignSelf: "center"
                      }}
                    >
                      INFO PAKET
                    </Text>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </Modal>
          </View>
        </ScrollView>
      </View>
      <Modal
          animationType="slide"
          transparent
          visible={modalMateri}
          onRequestClose={() => {
            setModalMateri(!modalMateri);
          }}
        >
          <View
            style={{ backgroundColor: "blue", height: "100%", width: "100%" }}
          >
            <WebView
              source={{ uri: linkMateri }}
              style={{
                height: "100%",
                width: "100%"
              }}
              userAgent={DeviceInfo.getUserAgent() + " - siapasn - android "}  
            />
          </View>
        </Modal>
    </SafeAreaView>
  );
};
export default Home;
