import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import MathJax from "react-native-mathjax";
import { useNavigation } from "@react-navigation/native";
import CountDown from "react-native-countdown-component";
import HTML from "react-native-render-html";
import { useDispatch, useSelector } from "react-redux";
import { heightPercent, widthPercent } from "../../utils";
import { Reducers } from "../../redux/types";
import FontType from "../../assets/fonts";
import height from "../../utils/heightPercent";
import { newhost } from "../../configs/api";

const Tryout = ({ route }) => {
  const navigation = useNavigation();
  const login = useSelector((state: Reducers) => state.login);
  const lasttime = useRef();
  const [tryoutstate, setTryoutState] = useState({
    id: 11,
    Name: "Pedagogik",
    Passing_grade: 70,
    createdAt: "2022-02-20T06:16:45.024Z",
    updatedAt: "2022-03-02T12:35:24.185Z",
    publishedAt: "2022-02-20T06:16:45.021Z",
    Soal: [
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      },
      {
        id: 1,
        No: 1,
        Soal:
          "<p>Nilai dari [latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}[/latex] adalah....<br><br>&nbsp;</p>",
        Pembahasan:
          "<p>Pembahasan :</p><p>[latex]\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2\\\\sqrt{2}}}}=\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}^{2}=2\\\\text{a}[/latex]</p><p>[latex]\\\\text{a}-2\\\\text{a}=0[/latex]</p><p>[latex]\\\\text{a}\\\\left(\\\\text{a}-2\\\\right)=0[/latex]</p><p>a=0 dan a=2</p><p>&nbsp;</p><p>Jawaban yang benar : D</p>",
        jawabanUser: null,
        Jawaban: [
          {
            Pilihan: "A",
            Jawaban:
              "<p>A. [latex]\\\\frac{1}{2}\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 4
          },
          {
            Pilihan: "B",
            Jawaban: "<p>B. [latex]\\\\sqrt{2}[/latex]<br>&nbsp;</p>",
            Skor: 2
          },
          {
            Pilihan: "C",
            Jawaban: "<p>C. 1<br>&nbsp;</p>",
            Skor: 1
          },
          {
            Pilihan: "D",
            Jawaban: "<p>D. 2<br>&nbsp;</p>",
            Skor: 3
          }
        ]
      }
    ],
    Deskripsi_tryout: "Tryout PPPK Paket 1 Biologi",
    Deskripsi_exam: "Lorem",
    Durasi: "01:00:00.000"
  });
  const dispatch = useDispatch();
  const [counter, setCounter] = useState(0);
  const [refresh, setRefresh] = useState(false);
  const [loading, setLoading] = useState(false);
  const [screenHeight, setScreenHeight] = useState(0);
  // const _finishExam = () => {
  //   dispatch(PostAnswer(answer, navigation, lasttime.current.state.lastUntil));
  // };
  const _handleGetExam = async () => {
    setLoading(true);
    newhost
      .get(
        `/api/access-exam?tryout=${route.params.NameTryout}&exam=${
          route.params.NameExam
        }`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${login.jwttoken}`
          }
        }
      )
      .then(res => {
        setTryoutState(res.data.exam);
        setLoading(false);
      })
      .catch(res => {
        setLoading(false);
        if (res.response.data.error.status === 403) {
          Alert.alert("Gagal", "Anda Tidak Memiliki Akses ke Tryout ini", [
            { text: "OK", onPress: () => navigation.goBack() }
          ]);
        }
      });
  };
  useEffect(() => {
    _handleGetExam();
  }, []);
  const _handleJawab = (Answer: string) => {
    const datares = tryoutstate;
    datares.Soal[counter].jawabanUser = Answer;
    setTryoutState(datares);
    setRefresh(!refresh);
  };
  const _handleNomorClick = (index: any) => {
    setCounter(index);
  };
  // const _handleBookmark = (index: any) => {
  //   setCounter(index);
  //   const datares = [...jawaban];
  //   if (datares[counter].bookmark) {
  //     datares[counter].bookmark = false;
  //   } else {
  //     datares[counter].bookmark = true;
  //   }

  //   setJawaban(datares);
  // };

  const _conditionalstyleAnswer = (answer: string) => {
    if (answer === tryoutstate.Soal[counter].jawabanUser) {
      return {
        width: 20,
        height: 20,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: "#C2CFE0",
        backgroundColor: "#C2CFE0",
        alignItems: "center",
        justifyContent: "center",
        marginRight: 10
      };
    }
    return {
      width: 20,
      height: 20,
      borderRadius: 20,
      borderWidth: 1,
      borderColor: "#C2CFE0",
      backgroundColor: "white",
      alignItems: "center",
      justifyContent: "center",
      marginRight: 10
    };
  };
  const conditionalstyle = (index: any) => {
    // if (jawaban[index].bookmark) {
    //   return {
    //     backgroundColor: "yellow",
    //     borderRadius: 5,
    //     borderColor: "white",
    //     alignItems: "center",
    //     justifyContent: "center",
    //     margin: 2,
    //     marginHorizontal: 5,
    //     width: 35,
    //     height: 35
    //   };
    // }
    if (counter === index) {
      return {
        backgroundColor: "#0B7BF9",
        borderColor: "#0B7BF9",
        borderRadius: 100,
        alignItems: "center",
        justifyContent: "center",
        margin: 2,
        marginHorizontal: 10,
        width: 35,
        height: 35
      };
    }
    if (tryoutstate.Soal[index].jawabanUser) {
      return {
        backgroundColor: "#F5C114",
        borderColor: "#F5C114",
        borderRadius: 100,
        alignItems: "center",
        justifyContent: "center",
        margin: 2,
        marginHorizontal: 10,
        width: 35,
        height: 35
      };
    }
    return {
      backgroundColor: "#BEBEBE",
      borderColor: "#BEBEBE",
      borderRadius: 100,
      alignItems: "center",
      justifyContent: "center",
      margin: 2,
      marginHorizontal: 10,
      width: 35,
      height: 35
    };
  };
  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={conditionalstyle(index)}
        onPress={() => _handleNomorClick(index)}
      >
        <Text
          style={{
            fontSize: 16,
            fontWeight: "bold",
            color: counter === index ? "white" : "#5D5C5D"
          }}
        >
          {index + 1}
        </Text>
      </TouchableOpacity>
    );
  };
  const _Next = (_nomor: any) => {
    if (tryoutstate.Soal.length > _nomor) {
      setCounter(_nomor);
    }
  };
  const _Back = (nomor: any) => {
    if (nomor >= 0) {
      setCounter(nomor);
    }
  };
  const _handleTime = (time: any) => {
    const arrayTime = time.split(":");
    const arraySeccond = arrayTime[2].split(".");
    return arrayTime[0] * 3600 + arrayTime[1] * 60 + arraySeccond[0];
  };
  const onContentSizeChange = (contentWidth: any, contentHeight: any) => {
    setScreenHeight(contentHeight);
  };
  const scrollEnabled = screenHeight > heightPercent(98);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F7F7F7" }}>
      <StatusBar barStyle="light-content" backgroundColor="#468189" />
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
        scrollEnabled={scrollEnabled}
        onContentSizeChange={onContentSizeChange}
        showsVerticalScrollIndicator={false}
      >
        {loading ? (
          <View>
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <View
            style={{
              flexGrow: 1,
              justifyContent: "space-between",
              padding: 10
            }}
          >
            <View>
              <View
                style={{
                  height: 40,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginHorizontal: 10
                }}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text
                    style={{
                      color: "#5D5C5D",
                      fontSize: 14,
                      fontFamily: FontType.ExtraBold
                    }}
                  >
                    Sisa Waktu:
                    {"  "}
                  </Text>
                  <CountDown
                    ref={lasttime}
                    until={_handleTime(tryoutstate.Durasi)}
                    size={14}
                    timeLabels={{ m: null, s: null }}
                    digitStyle={{ backgroundColor: "#F7F7F7" }}
                    digitTxtStyle={{ color: "#5D5C5D" }}
                    timeToShow={["M", "S"]}
                    // onFinish={() => _finishExam()}
                    showSeparator
                    separatorStyle={{ color: "#5D5C5D" }}
                  />
                </View>
                <TouchableOpacity
                  style={{ alignItems: "center", justifyContent: "center" }}
                  // onPress={() => _finishExam()}
                >
                  <View
                    style={{
                      borderRadius: 20,
                      padding: 16,
                      paddingVertical: 4,
                      marginTop: 5,
                      backgroundColor: "#F5C114"
                    }}
                  >
                    <Text
                      style={{
                        color: "#152C07",
                        fontSize: 14,
                        fontFamily: FontType.ExtraBold
                      }}
                    >
                      Selesai
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  maxHeight: height(18),
                  marginVertical: 7,
                  backgroundColor: "white",
                  borderRadius: 10,
                  padding: 10,
                  marginHorizontal: 10
                }}
              >
                <Text
                  style={{
                    color: "Text/Gray",
                    fontSize: 13,
                    fontFamily: FontType.bold,
                    marginBottom: 5
                  }}
                >
                  NO SOAL:
                </Text>
                <FlatList
                  data={tryoutstate.Soal}
                  renderItem={renderItem}
                  keyExtractor={(item, index) => index.toString()}
                  numColumns={6}
                  showsHorizontalScrollIndicator={false}
                  showsVerticalScrollIndicator={false}
                  columnWrapperStyle={{ flexWrap: "wrap" }}
                />
              </View>
            </View>
            <View
              style={{
                borderRadius: 5,
                flex: 1,
                backgroundColor: "white",
                margin: 10,
                padding: 15
              }}
            >
              {/* <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <Text style={{ textAlign: "center" }}>
                {"Pertanyaan "
                  .concat(counter + 1)
                  .concat(" dari ")
                  .concat(tryoutstate.Soal.length)}
              </Text>
              <TouchableOpacity onPress={() => _handleBookmark(counter)}>
                <Text>Bookmark</Text>
              </TouchableOpacity>
            </View> */}
              {/* <MathJax
              // HTML content with MathJax support
              html={tryoutstate.Soal[counter].Soal}
              // MathJax config option
              mathJaxOptions={{
                messageStyle: "none",
                extensions: ["tex2jax.js"],
                jax: ["input/TeX", "output/HTML-CSS"],
                tex2jax: {
                  inlineMath: [["[latex]", "[/latex]"], ["[", "["]],
                  displayMath: [["[latex]", "[/latex]"], ["[", "["]],
                  processEscapes: true
                },
                TeX: {
                  extensions: [
                    "AMSmath.js",
                    "AMSsymbols.js",
                    "noErrors.js",
                    "noUndefined.js"
                  ]
                }
              }}
            /> */}
              <View>
                <FlatList
                  data={tryoutstate.Soal[counter].Jawaban}
                  style={{
                    maxHeight: height(25),
                    marginVertical: 7,
                    backgroundColor: "white",
                    borderRadius: 10,
                    padding: 10,
                    marginHorizontal: 10
                  }}
                  extraData={refresh}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity
                      key={index + 1}
                      onPress={() => {
                        _handleJawab(item.Pilihan);
                      }}
                      style={{ flexDirection: "row" }}
                    >
                      <View style={_conditionalstyleAnswer(item.Pilihan)} />
                      <Text>{item.Jawaban}</Text>
                    </TouchableOpacity>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                  showsHorizontalScrollIndicator={false}
                />
              </View>
            </View>
            <View
              style={{
                height: 40,
                flexDirection: "row",
                marginHorizontal: 10
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  _Back(counter - 1);
                }}
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "#F5C114",
                  borderRadius: 4
                }}
              >
                <Text
                  style={{
                    color: "#152C07",
                    fontSize: 12,
                    fontFamily: FontType.ExtraBold
                  }}
                >
                  Sebelumnya
                </Text>
              </TouchableOpacity>
              <View
                style={{
                  width: 50,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "#F7F7F7",
                  borderRadius: 10,
                  marginBottom: 5
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  _Next(counter + 1);
                }}
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#F5C114",
                  borderRadius: 4
                }}
              >
                <Text
                  style={{
                    color: "#152C07",
                    fontSize: 12,
                    fontFamily: FontType.ExtraBold
                  }}
                >
                  Berikutnya
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Tryout;
