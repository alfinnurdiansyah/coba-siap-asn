import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  body: {
    margin: 10,
    marginBottom: 0,
    flex: 1
  },
  bodytryout: {
    marginBottom: 0,
    flex: 1
  }
});

export default styles;
